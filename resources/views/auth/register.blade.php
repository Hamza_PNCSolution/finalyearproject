<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="../../../../">
		<meta charset="utf-8" />
		<title>Final Year Management System</title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="//keenthemes.com/metronic" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Custom Styles(used by this page)-->
		<link href="fyp/assets/css/pages/login/classic/login-6.css" rel="stylesheet" type="text/css" />
		<!--end::Page Custom Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="fyp/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="fyp/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
		<link href="fyp/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<link href="fyp/assets/css/themes/layout/header/base/light.css" rel="stylesheet" type="text/css" />
		<link href="fyp/assets/css/themes/layout/header/menu/light.css" rel="stylesheet" type="text/css" />
		<link href="fyp/assets/css/themes/layout/brand/dark.css" rel="stylesheet" type="text/css" />
		<link href="fyp/assets/css/themes/layout/aside/dark.css" rel="stylesheet" type="text/css" />
		<!--end::Layout Themes-->
		<link rel="shortcut icon" href="fyp/assets/media/logos/favicon.ico" />
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Login-->
			<div class="login login-6 login-signin-on login-signin-on d-flex flex-column-fluid" id="kt_login">
				<div class="d-flex flex-column flex-lg-row flex-row-fluid text-center" style="background-image: url(fyp/assets/media/bg/bg-3.jpg);">
					<!--begin:Aside-->
					<div class="d-flex w-100 flex-center p-15">
						<div class="login-wrapper">
							<!--begin:Aside Content-->
							<div class="text-dark-75">
								<a href="#">
									<img src="fyp/assets/media/logos/logo-letter-13.png" class="max-h-75px" alt="" />
								</a>
								<h3 class="mb-8 mt-22 font-weight-bold">JOIN OUR GREAT COMMUNITY</h3>
								<a href="{{ route('login') }}" type="button" class="btn btn-outline-primary btn-pill py-4 px-9 font-weight-bold">Already Account</a>
							</div>
							<!--end:Aside Content-->
						</div>
					</div>
					<!--end:Aside-->
					<!--begin:Divider-->
					<div class="login-divider">
						<div></div>
					</div>
					<!--end:Divider-->
					<!--begin:Content-->
					<div class="d-flex w-100 flex-center p-15 position-relative overflow-hidden">
						<div class="login-wrapper">
							<!--begin:Sign In Form-->
							<div class="login-signin">
								<div class="text-center mb-10 mb-lg-20">
									<h2 class="font-weight-bold">Sign Up</h2>
                                    <p class="text-muted font-weight-bold">Enter your account details</p>
                                </div>
                                <!-- Validation Errors -->
                                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                                <form class="form text-left" method="POST" action="{{ route('register') }}">
                                    @csrf
									<div class="form-group py-2 m-0">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75"id="name"type="text" name="name" :value="old('name')" required autofocus placeholder="Username"/>
									</div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75" id="email" type="email" name="email" :value="old('email')" required placeholder="Email"/>
									</div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75"  id="password" type="password" name="password" required autocomplete="new-password" placeholder="Password"/>
									</div>
									<div class="form-group py-2 m-0 border-top">
										<input class="form-control h-auto border-0 px-0 placeholder-dark-75" id="password_confirmation" type="password" name="password_confirmation" required placeholder="Confirm password">
									</div>
									<div class="form-group d-flex flex-wrap flex-center">
										<button type="submit" class="btn btn-primary btn-pill font-weight-bold px-9 py-4 my-3 mx-2">{{ __('Register') }}</button>
									</div>
								</form>
							</div>
							<!--end:Sign In Form-->
						</div>
					</div>
					<!--end:Content-->
				</div>
			</div>
			<!--end::Login-->
		</div>
		<!--end::Main-->
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="fyp/assets/plugins/global/plugins.bundle.js"></script>
		<script src="fyp/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
		<script src="fyp/assets/js/scripts.bundle.js"></script>
		<!--end::Global Theme Bundle-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="fyp/assets/js/pages/custom/login/login-general.js"></script>
		<!--end::Page Scripts-->
	</body>
	<!--end::Body-->
</html>
