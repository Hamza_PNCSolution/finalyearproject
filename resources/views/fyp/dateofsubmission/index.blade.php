@extends('fyp.layouts.app')
@section('style ')
<style>
span.select2.select2-container.select2-container--default.select2-container--focus {
    width: 100%;
}
</style>
@endsection
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Set Date of Submission and it's Response</h3>
        </div>
        {{-- @dd(count($dateofsubmission)) --}}
        @if (count($dateofsubmission) != 2)
            <div class="card-toolbar">
                <!--begin::Dropdown-->
                <div class="dropdown dropdown-inline mr-2">
                    <button type="submit" class="btn btn-light-primary font-weight-bolder"  data-toggle="modal" data-target="#NewMember">
                        <span class="svg-icon svg-icon-md">
                            <i class="fas fa-plus-circle"></i>
                        </span>Set Date & Time</button>
                </div>
                <!--end::Dropdown-->
            </div>
        @endif
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th >Sr No</th>
                    <th>Date</th>
                    <th>Purpose</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dateofsubmission as $key => $data)
                <tr>
                    <td >{{++$key}}</td>
                    <td>{{$data->date}}</td>
                    <td>{{($data->type == 1) ? 'Last Date of Submission' : 'Last Date of Submission Response'  }}</td>
                    <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                        <a href="javascript:void(0)" onclick="editDate({{$data->id}})" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                        </span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>dateofsubmission
  <!-- Modal -->
  <div class="modal fade" id="NewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">New Date of Submission</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('store.date') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-3 col-form-label">
                        <label>Date & Time</label>
                    </div>
                    <div class="col-lg-9">
                        <input type="text" id="datepicker" name="date" class="form-control" style="width: 100% !important;" placeholder="Please select the data" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-3 col-form-label">
                        <label>Purpose</label>
                    </div>
                    <div class="col-lg-9">
                        <select name="type" class="form-control" required>
                            <option value="">Please select type of data and time</option>
                            @if (isset($dateoftype))
                                <option value="1" {{ ($dateoftype->type == '1') ? 'disabled' : null  }}>Last Date of Proposal Submission</option>
                                <option value="2" {{ ($dateoftype->type == '2') ? 'disabled' : null  }}>Last Date of Proposal Response</option>
                            @else
                                <option value="1">Last Date of Proposal Submission</option>
                                <option value="2">Last Date of Proposal Response</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Update Modal -->
  <div class="modal fade" id="UpdateDate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Update Date of Submission</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('update.date') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-lg-12">
                        <input type="hidden" name="id" id="id" value=""/>
                        <input type="text" id="datepicker2" name="date" class="form-control" style="width: 100% !important;" value="" required/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('footer.scripts')
<script>
    function editDate(id){
        $.get('editdate/'+id,function(data){
            if(data){
                console.log(data.date);
                $("#id").val(id);
                $("#datepicker2").val(data.date);
            }
            $("#UpdateDate").modal('toggle');

        })
    }

    $( function() {
      $( "#datepicker , #datepicker2" ).datepicker({
          minDate: 0,
            beforeShowDay: function(date) {
            var show = true;
            if(date.getDay()==6 || date.getDay()==0) show=false
            return [show];
            }
        });
    } );
</script>
@endsection
