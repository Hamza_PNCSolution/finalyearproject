@extends('fyp.layouts.app')
@section('content')
<div class="d-flex flex-column" id="kt_wrapper">

    @extends('fyp.layouts.head')

    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                @hasrole('Admin')
                <!--Begin::Row-->
                <div class="row">
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 25-->
                        <div class="card card-custom bg-light-success card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-success">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
                                    <i class="fas fa-user-graduate icon-2x text-success "></i>
                                    <!--end::Svg Icon-->
                                </span>
                                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$student}}</span>
                                <span class="font-weight-bold text-muted font-size-sm">Total Students</span>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 25-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 26-->
                        <div class="card card-custom bg-light-danger card-stretch gutter-b">
                            <!--begin::ody-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-danger">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
                                    <i class="fas fa-chalkboard-teacher icon-2x text-danger"></i>
                                    <!--end::Svg Icon-->
                                </span>
                                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$teacher}}</span>
                                <span class="font-weight-bold text-muted font-size-sm">Total Teachers</span>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 26-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 27-->
                        <div class="card card-custom bg-light-info card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-info">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Media/Equalizer.svg-->
                                    <i class="fas fa-users icon-2x text-info "></i>
                                    <!--end::Svg Icon-->
                                </span>
                                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$groups}}</span>
                                <span class="font-weight-bold text-muted font-size-sm">Total Groups</span>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 27-->
                    </div>
                    <div class="col-xl-3">
                        <!--begin::Stats Widget 28-->
                        <div class="card card-custom bg-light-warning card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body">
                                <span class="svg-icon svg-icon-2x svg-icon-warning">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group-chat.svg-->
                                    <i class="fas fa-user-tie icon-2x text-warning "></i>
                                    <!--end::Svg Icon-->
                                </span>
                                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $committee }}</span>
                                <span class="font-weight-bold text-muted font-size-sm">Total Committee Members</span>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stat: Widget 28-->
                    </div>
                </div>
                <!--End::Row-->
                <div class="row">
                    {{-- All Groups List --}}
                    <div class="col-xl-6">
                        <!--begin::List Widget 12-->
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0">
                                <h3 class="card-title font-weight-bolder text-dark">Recent Groups</h3>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">
                                <!--begin::Item-->
                                @foreach ($grouplist as $groups)
                                    <div class="d-flex flex-wrap align-items-center mb-10">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 ">
                                            <div class="symbol-label rounded-circle" style="background-image: url('fyp/assets/media/stock-600x400/group-icon.png'); height:60px; width:60px;"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column ml-4 flex-grow-1 mr-2" style="width: 55%;">
                                            <a href="{{ route('detials.groups', $groups->id) }}" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">{{ $groups->title }}</a>
                                            <span class="text-muted font-weight-bold">{{ \Illuminate\Support\Str::limit($groups->description, 20, $end='...') }}</span>
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::btn-->
                                        <span class="label label-lg @if ($groups->is_approved == 1)
                                            label-light-primary
                                        @elseif($groups->is_rejected == 1)
                                            label-light-danger
                                        @elseif($groups->is_rejected == 0 && $groups->is_approved == 0)
                                            label-light-warning
                                        @endif
                                            label-light-primary label-inline mt-lg-0 mb-lg-0 my-2 font-weight-bold py-4">
                                            @if ($groups->is_approved == 1)
                                                Approved
                                            @elseif($groups->is_rejected == 1)
                                                Rejected
                                            @elseif($groups->is_rejected == 0 && $groups->is_approved == 0)
                                                Pending
                                            @endif
                                        </span>
                                        <!--end::Btn-->
                                    </div>
                                @endforeach
                                <!--end::Item-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 12-->
                    </div>
                    <div class="col-xl-6">
                        <!--begin::List Widget 13-->
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0">
                                <h3 class="card-title font-weight-bolder text-dark">Recent Activities</h3>
                            </div>
                        <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">
                                <!--begin::Item-->
                                @foreach ($activities as $activity)
                                    <div class="d-flex flex-wrap align-items-center mb-10">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 ">
                                            <div class="symbol-label rounded-circle" style="background-image: url('fyp/assets/media/stock-600x400/activity.jpeg'); width:60px; height:60px;"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column ml-4 flex-grow-1 mr-2">
                                            <a href="{{ route('detials.groups', $activity->id) }}" class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">{{ $activity->title }}</a>
                                            <span class="text-muted font-weight-bold">{{ \Illuminate\Support\Str::limit($activity->description, 20, $end='...') }}</span>
                                        </div>
                                        <!--end::Title-->
                                        <!--begin::btn-->
                                        <span class="label label-lg @if ($activity->status == 'Assigned')
                                            label-light-primary
                                        @elseif($$activity->status == 'Submit')
                                            label-light-success
                                        @endif
                                            label-light-primary label-inline mt-lg-0 mb-lg-0 my-2 font-weight-bold py-4">
                                            @if ($activity->status == 'Assigned')
                                                Assigned
                                            @elseif($$activity->status == 'Submit')
                                                Submitted
                                            @endif
                                        </span>
                                        <!--end::Btn-->
                                    </div>
                                @endforeach
                                <!--end::Item-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 13-->
                    </div>
                </div>
                @endhasrole
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
</div>
@endsection
