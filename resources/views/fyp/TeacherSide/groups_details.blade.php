@extends('fyp.layouts.app')
@section('content')
<div class="d-flex flex-column" id="kt_wrapper">

    @extends('fyp.layouts.head')

    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::List Widget 2-->
                        <div class="card card-custom bg-light-success card-stretch gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-6 mb-2">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label font-weight-bold font-size-h4 text-dark-75 mb-3">{{ $groupdata->title}}</span>
                                    <span class="text-muted font-weight-bold font-size-sm">{{ $groupdata->description}}</span>
                                </h3>
                                <h5 class="card-title align-items-center flex-column" style="font-size: 14px!important;">
                                    <span class="font-weight-bold pt-3">{{ (isset($datetime)) ? 'Last Date:  '.$converteddate ?? '' : '' }}</span></h5>
                                <div class="card-toolbar">
                                    <button class="btn btn-light-primary btn-sm btn-shadow font-weight-bolder font-size-sm" data-toggle="modal" data-target="#projectporposal">
                                        Project Proposal &nbsp;<i class="fas fa-eye"></i>
                                    </button>
                                    &nbsp;
                                    &nbsp;
                                        <a href="{{ asset($groupdata->path)}}" download class="btn btn-light-warning btn-sm btn-shadow font-weight-bolder font-size-sm">
                                            Project Proposal &nbsp;<i class="fas fa-file-download"></i>
                                        </a>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">
                                @foreach ($students as $student)
                                <!--begin::Item-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::Symbol-->
                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                        <div class="symbol-label">
                                            <img src="{{ URL:: asset('fyp/assets/media/svg/avatars/004-boy-1.svg')}}" class="h-75 align-self-end" alt="" />
                                        </div>
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Text-->
                                    <div class="d-flex flex-column font-weight-bold">
                                        <button class="btn text-dark text-hover-primary font-size-lg" type="button" data-toggle="collapse" data-target="#collapse-{{$student->id}}" aria-expanded="true" aria-controls="collapse-{{$student->id}}" style="padding: 0px;">{{ $student->name}}</button>
                                        <span class="text-muted">{{$student->roll_no}}</span>
                                    </div>
                                    <!--end::Text-->
                                </div>
                                <!--end::Item-->
                                <div class="accordion mb-5" id="accordionExample">
                                    <div class="card">
                                      <div id="collapse-{{$student->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                          <div class="row">
                                              <div class="col-md-2">
                                                  <label class="card-label font-weight-bold mb-3">Enrollment No</label><br>
                                                  <label class="card-label font-weight-bold mb-3">CGPA</label><br>
                                                  <label class="card-label font-weight-bold mb-3">Email</label><br>
                                                  <label class="card-label font-weight-bold mb-3">Phone</label>
                                              </div>
                                              <div class="col-md-10">
                                                  <label class="card-label font-weight-medium mb-3">{{$student->enrollment_no}}</label><br>
                                                  <label class="card-label font-weight-medium mb-3">{{$student->cgpa}}</label><br>
                                                  <label class="card-label font-weight-medium mb-3">{{$student->email}}</label><br>
                                                  <label class="card-label font-weight-medium mb-3">{{$student->phone}}</label>
                                              </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                @endforeach
                                <div class="card-toolbar">
                                    {{-- >= --}}
                                    @if ($converteddate >= now()->timezone('asia/karachi')->format('Y-m-d h:i:s A'))
                                        @if ( $checkgroup->is_approved != 0 || $checkgroup->is_rejected != 0 )
                                            <p class="text-center font-weight-bolder font-size-h3"><span class="text-dark font-weight-bolder">Note:&nbsp;&nbsp;</span>Proposal Response is submitted</p>
                                        @else
                                        <a href="#" class="btn btn-light-danger btn-sm btn-shadow font-weight-bolder font-size-sm float-right mx-3" data-toggle="modal" data-target="#RejectModal" id="reject">
                                            Reject &nbsp;<i class="fas fa-exclamation-circle"></i></a>
                                        <a href="#" class="btn btn-light-success btn-sm btn-shadow font-weight-bolder font-size-sm float-right active" data-toggle="modal" data-target="#ApprovedModal" id="approved">
                                            Approved &nbsp;<i class="fas fa-check-circle"></i></a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 2-->
                    </div>
                </div>
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
</div>

{{-- PDF Modal --}}
<!-- Modal-->
<div class="modal fade" id="projectporposal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Project Porposal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <iframe src ="{{ asset($groupdata->path)}}" width="100%" height="500px"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Comment Modal -->
<div class="modal fade" id="RejectModal" tabindex="-1" role="dialog" aria-labelledby="CommentModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Any Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{ route('group.rejected', $groupdata->id) }}">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col md-12">
                        <input type="hidden" name="for" value="reject">
                        <textarea class="form-control" name="comment" placeholder="Post a comment"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-light-danger btn-sm btn-shadow font-weight-bolder font-size-sm float-right mx-3">Reject &nbsp;<i class="fas fa-exclamation-circle"></i></button>
            </div>
        </form>
      </div>
    </div>
  </div>

<div class="modal fade" id="ApprovedModal" tabindex="-1" role="dialog" aria-labelledby="CommentModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Any Comment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="POST" action="{{ route('group.approved', $groupdata->id) }}">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col md-12">
                        <select class="form-control" name="proposal">
                            <option value="">Please Select Any Proposal</option>
                            @foreach ($proposals as $proposal)
                                <option value="{{$proposal->id}}">{{$proposal->title}}</option>
                            @endforeach
                        </select>
                        <br>
                        <input type="hidden" name="for" value="approved">
                        <textarea class="form-control" name="comment" rows="4" placeholder="Post a comment"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-light-success btn-sm btn-shadow font-weight-bolder font-size-sm float-right active">Approved &nbsp;<i class="fas fa-check-circle"></i></button>
            </div>
        </form>
        </div>
    </div>
</div>

@endsection
@section('footer.scripts')
<script>

</script>
@endsection
