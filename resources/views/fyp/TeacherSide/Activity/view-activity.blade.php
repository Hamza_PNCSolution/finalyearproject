@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Submitted Activity
        </h3>
    </div>
    <!--begin::Form-->
    <form action="#" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Activity Title</label>
                <div class="col-10">
                    <label class="col-2 col-form-label font-weight-bold">{{$activity->title}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Deadline Date</label>
                <div class="col-10">
                    <label class="col-2 col-form-label font-weight-bold">{{$activity->end_date}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Description</label>
                <div class="col-10">
                    <label class="col-2 col-form-label font-weight-bold">{{$activity->description}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Submitted Date</label>
                <div class="col-10">
                    <label class="col-2 col-form-label font-weight-bold">{{$activity->activitymeta->submitted_date ?? 'Not Submitted Yet'}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Comment</label>
                <div class="col-10">
                        <label class="col-2 col-form-label font-weight-bold">{{$activity->activitymeta->comment ?? 'No Comments'}}</label>
                </div>
            </div>

            @if ($activity->document == 'on')
                <div class="form-group row">
                    <label class="col-2 col-form-label font-weight-bolder">Activity Document</label>
                    <div class="col-10">
                        <a href="{{ asset('storage/'.$activity->activitymeta->document)}}"  class="btn btn-light-primary btn-sm btn-shadow font-weight-bolder font-size-sm" target="_blank"> View Document &nbsp;<i class="fas fa-eye"></i></a>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>
@endsection
@section('footer.scripts')
<script>
</script>
@endsection
