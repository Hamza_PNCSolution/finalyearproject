@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            New Activity
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('store.activity') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">

            <div class="form-group row">
                <label class="col-2 col-form-label">Activity Title</label>
                <div class="col-10">
                    <input class="form-control" type="title" id="" name="title" id="example-text-input" placeholder="Enter activity title" required/>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Group Name</label>
                <div class="col-10">
                    <select class="form-control select2" id="groupselect" name="group" multiple="multiple" required>
                        @foreach($groups as $group)
                            <option value="{{$group->id}}">{{$group->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Deadline Date</label>
                <div class="col-10">
                    <input type="date" class="form-control" name="end_date" placeholder="Enter Deadline Date" required/>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Description</label>
                <div class="col-10">
                    <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
                </div>
            </div>

            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Document Upload</label>
                <div class="col-3">
                    <span class="switch switch-outline switch-icon switch-brand">
                     <label>
                      <input type="checkbox" checked="checked" name="document"/>
                      <span></span>
                     </label>
                    </span>
                </div>
            </div>
            {{-- <div class="form-group row">
                <label class="col-2 col-form-label">Project Proposal</label>
                <div class="col-10">
                    <input type="file" name="proposal" class="dropify" data-allowed-file-extensions="pdf"  data-max-file-size-preview="3M"
                    data-show-errors="true" />
                </div>
            </div> --}}

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Save</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('footer.scripts')
<script>
    $(document).ready(function() {
        $('.select2').select2();

        // limiting the number of selections
        $('#groupselect').select2({
         placeholder: "Select an option",
         maximumSelectionLength: 4
        });

        });
</script>
@endsection
