@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Activity Details
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            @hasrole('Teacher')
            <a href="{{ route('new.activity') }}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Activity</a>
            <!--end::Button-->
            @endrole
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Sear   ch..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
            <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
            <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th>Sr No</th>
                    <th>Group Name</th>
                    <th>Activity Title</th>
                    <th>Description</th>
                    <th>Document Upload</th>
                    <th>End Date</th>
                    <th>Activity Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($activity as $key => $data)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$data->group->title}}</td>
                    <td>{{$data->title}}</td>
                    <td>{{$data->description}}</td>
                    <td>{{ ($data->document == null ) ? 'Off' : 'On'}}</td>
                    <td>{{ $data->end_date}}</td>
                    <td>{{ $data->status}}</td>
                    <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                        <span style="overflow: visible; position: relative; width: 125px;">
                            @if ($data->status == 'Assigned')
                            @hasrole('Teacher')
                            <a href="{{ route('edit.activity', $data->id) }}" class="btn btn-sm btn-clean btn-icon mr-2"
                                title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            @endhasrole
                            @hasrole('Admin')
                            <a href="@hasrole('Admin') {{ route('details.activity', $data->id) }}  @else {{ route('view.thactivity', $data->id) }} @endhasrole"
                                class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-eye"></i>
                                </span>
                            </a>
                            @endhasrole
                            @else
                            <a href="@hasrole('Admin') {{ route('details.activity', $data->id) }}  @else {{ route('view.thactivity', $data->id) }} @endhasrole"
                                class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-eye"></i>
                                </span>
                            </a>
                            @endif
                        </span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
