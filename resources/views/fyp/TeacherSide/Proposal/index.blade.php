@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">My Proposals</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#ProposalModal">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Proposal</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th>Sr No</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($proposals as $index => $data)
                <tr>
                    <td>{{++$index}}</td>
                    <td>{{$data->title}}</td>
                    <td>{{$data->description}}</td>
                    <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                        <a href="javascript:void(0)" onclick="editproposal({{$data->id}})" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                            <span class="svg-icon svg-icon-md">
                                <i class="fas fa-pen"></i>
                            </span>
                        </a>
                        <a href="{{ route('delete.proposal', $data->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Delete">
                            <span class="svg-icon svg-icon-md">
                                <i class="fas fa-trash"></i>
                            </span>
                        </a>
                        </span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ProposalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-center" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Proposal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <form name="form1">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input class="form-control" type="text" name="title" id="title" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" type="text" name="description" id="description" rows="8" cols="50" required></textarea>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-form-submit">Save</button>
            </div>
        </div>
    </div>
</div>
{{-- Update Modal --}}
<div class="modal fade" id="UpdateProposalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-center" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" type="hidden" id="id"/>
                                <input class="form-control" type="text" name="title" id="titleed" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" type="text" name="description" id="descriptioned" rows="8" required>
                                </textarea>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-form-update">Update</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer.scripts')
<script>

    $(".btn-form-submit").on('click', function(e) {
        e.preventDefault();

        var formData = $("form[name=form1]").serialize();
        console.log(formData);
        $.post("{{ route('store.proposal') }}", {
                 _token: "{{ csrf_token() }}",
                form : formData
            }).then(function (response){
                $("#ProposalModal").modal('hide');
                toastr.success("Proposal has been Added");
                setTimeout(function () { window.location.href = "{{ route('all.proposal')}}";}, 1500);
        })

    });

    function editproposal(id){
        $.get('editproposal/'+id,function(data){
            // console.log(data.id);
            $("#id").val(data.id);
            $("#titleed").val(data.title);
            $("#descriptioned").val(data.description);
            $("#UpdateProposalModal").modal('toggle');

        })
    }

    $(".btn-form-update").on('click', function(e) {
      let id = $("#id").val();
      let title = $("#titleed").val();
      let description = $("#descriptioned").val();

      $.ajax({
          url: '/updateproposal/' + id,
          type:"Post",
          data:{
              id:id,
              title:title,
              description:description,
              _token:'{{ csrf_token() }}'
          },
          success:function(response){
              if(response){
                  $("#UpdateProposalModal").modal('hide');
                  toastr.success("Proposal has been Updated.");
                  setTimeout(function(){ window.location.href = "{{ route('all.proposal') }}"; }, 1500);

              }
          }
      })

    });
</script>
@endsection
