@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Teacher Meeting Timing</h3>
        </div>
        <div class="card-toolbar">
            @if(auth()->user()->hasrole('Teacher'))
            <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#SlotModal">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Slot</a>
            @endif
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th >Sr No</th>
                    <th title="Field #4">Date</th>
                    <th title="Field #5">Start Time</th>
                    <th title="Field #4">End Time</th>
                    <th title="Field #2">Room</th>
                    @if(auth()->user()->hasrole('Teacher'))
                        <th title="Field #6">Action</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($slots as $index => $data)
                <tr>
                    <td >{{++$index}}</td>
                    <td>{{$data->date}}</td>
                    <td class="text-right">{{$data->start_time}}</td>
                    <td class="text-right">{{$data->end_time}}</td>
                    <td class="text-right">{{$data->room}}</td>
                    @if (auth()->user()->hasrole('Teacher'))
                        <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                            <span style="overflow: visible; position: relative; width: 125px;">
                            <a href="javascript:void(0)" onclick="editslots({{$data->id}})" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            <a href="{{ route('delete.slots', $data->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Delete">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-trash"></i>
                                </span>
                            </a>
                            </span>
                        </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="SlotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-center" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body repeater">
                <div data-repeater-list="products">
                    <form name="form1">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input class="form-control" type="date" name="date" id="date" required/>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input class="form-control" type="time" name="starttime" id="startname" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input class="form-control" type="time" name="endtime" id="endtime" required/>

                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Room</label>
                                    <input class="form-control" type="text" name="room" id="room" required/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-form-submit">Save</button>
            </div>
        </div>
    </div>
</div>
{{-- Update Modal --}}
<div class="modal fade" id="UpdateSlotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-center" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
            <div class="modal-body">
                <div data-repeater-list="products">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Date</label>
                                <input class="form-control" type="hidden" id="id"/>
                                <input class="form-control" type="date" name="updatedate" id="dateET" required/>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Start Time</label>
                                <input class="form-control" type="time" name="starttime" id="starttimeET" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>End Time</label>
                                <input class="form-control" type="time" name="endtime" id="endtimeET" required/>

                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Room</label>
                                <input class="form-control" type="text" name="room" id="roomET" required/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-form-update">Update</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer.scripts')
<script>

    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("date")[0].setAttribute('min', today);

    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("updatedate")[0].setAttribute('min', today);

    $(".btn-form-submit").on('click', function(e) {
        e.preventDefault();

        var formData = $("form[name=form1]").serialize();
            $.post("{{ route('store.slots') }}", {
                 _token: "{{ csrf_token() }}",
                form : formData
            }).then(function (response){
                $("#SlotModal").modal('hide');
                toastr.success("Slot has been Added");
                setTimeout(function () { window.location.href = "{{ route('all.slots')}}";}, 1500);
        })

    });

    function editslots(id){
        $.get('editslots/'+id,function(data){
            console.log(data[0]);
            $("#id").val(data[0].id);
            $("#dateET").val(data[0].date);
            $("#starttimeET").val(data[0].start_time);
            $("#endtimeET").val(data[0].end_time);
            $("#roomET").val(data[0].room);
            $("#UpdateSlotModal").modal('toggle');

        })
    }

    $(".btn-form-update").on('click', function(e) {
      let id = $("#id").val();
      let date = $("#dateET").val();
      let starttime = $("#starttimeET").val();
      let endtime = $("#endtimeET").val();
      let room = $("#roomET").val();

      $.ajax({
          url: '/updateslots/' + id,
          type:"Post",
          data:{
              id:id,
              date:date,
              starttime:starttime,
              endtime:endtime,
              room:room,
              _token:'{{ csrf_token() }}'
          },
          success:function(response){
              if(response){
                  $("#UpdateSlotModal").modal('hide');
                  toastr.success("Slots has been Updated.");
                  setTimeout(function(){ window.location.href = "{{ route('all.slots') }}"; }, 1500);

              }
          }
      })

    });
</script>
@endsection
