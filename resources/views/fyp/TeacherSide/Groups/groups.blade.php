@extends('fyp.layouts.app')
@section('content')
@if ($groups->count() > 0)
    <div class="row">
        @foreach ($groups as $index=> $data)
        <div class="col-xl-6">
            <!--begin::Stats Widget 1-->
            <div class="card card-custom bgi-no-repeat card-stretch gutter-b"
                style="background-position: right top; background-size: 30% auto; background-image: url(fyp/assets/media/svg/shapes/abstract-{{++$index}}.svg)">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <a href="javascript:void(0)" onclick="groupdetials({{$data->id}})" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">{{$data->title}}</a>
                        </div>
                        <div class="col-6">
                            <a href="{{ $data->path}}"  class="btn btn-light-primary btn-sm btn-shadow font-weight-bolder font-size-sm" target="_blank"> Project Proposal &nbsp;<i class="fas fa-eye">
                            </i></a>
                        </div>
                    </div>
                    <div class="font-weight-bold text-success mt-9 mb-5">In Progress</div>
                    <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">{{$data->description}}</p>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Stats Widget 1-->
        </div>
        @endforeach
    </div>
    @else
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center pt-5 mt-5">No Group Found</h1>
            </div>
        </div>
    </div>
    @endif
<!-- Modal-->
<div class="modal fade" id="groupdetials" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Group Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::List Widget 2-->
                        <div class="card card-custom bg-light-success">
                            <!--begin::Body-->
                            <div class="card-body pt-10">
                                <!--begin::Item-->
                                <div class="" id="student">

                                </div>
                                <!--end::Item-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 2-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer.scripts')
<script>

    function groupdetials(id) {
        $.get('groupdetials/'+id, function (data) {
            var stddata = data.students;
            console.log(data);
            $("#student").empty();

            var my_orders = $("#student");


            $.each(stddata, function(i,student){

                my_orders.append('<div class="d-flex align-items-center mb-10"><div class="symbol symbol-40 symbol-light-white mr-5"><div class="symbol-label"><img src="fyp/assets/media/svg/avatars/004-boy-1.svg" class="h-75 align-self-end" alt="" /></div></div><div class="d-flex flex-column font-weight-bold"><span class="text-dark text-hover-primary mb-1 font-size-lg">'+student.roll_no+'</span><span class="text-muted">'+student.name+'</span></div></div>');

            });
            $("#groupdetials").modal('toggle');

        });
    }

</script>
@endsection
