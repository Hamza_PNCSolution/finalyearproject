@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Panel Meeting
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="table table-separate table-responsive table-head-custom table-checkable table-sm table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Groups</th>
                    <th>Date</th>
                    <th>Meeting Type</th>
                    <th>Meeting Panel</th>
                    <th>Room</th>
                    <th>Start Time</th>
                    <th>Meeting Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($scheduler))
                    @php
                        $count = $teachercount / 2;
                    @endphp
                    @foreach ($scheduler as $key => $data)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$data->group->title}}</td>
                        <td>{{$data->meeting_date}}</td>
                        <td>{{$data->meeting_type}}</td>
                        <td>
                            @if (count($data->panelteacher) != 0)
                                @if ($key < 18)
                                    @if ($key < 9)
                                        @for ($i = 0; $i < $count; $i++)
                                            {!!$data->panelteacher[$i]->teacher->name.','.'</br>'!!}
                                        @endfor
                                    @else
                                        @for ($i = $count; $i < $teachercount; $i++)
                                            {!!$data->panelteacher[$i]->teacher->name.','.'</br>'!!}
                                        @endfor
                                    @endif
                                @endif
                            @else
                                {{'Committee Member'}}
                            @endif
                        </td>
                        <td>{{$data->room}}</td>
                        <td>
                            @if ($data->meeting_type == 'defense_one' || $data->meeting_type == 'defense_two')
                                {{ date("H:i a", strtotime($data->timeslot->defense))}}
                            @elseif($data->meeting_type == 'mid_semester')
                                {{ date("H:i a", strtotime($data->timeslot->mid_semester))}}
                            @elseif($data->meeting_type == 'mid_year' || $data->meeting_type == 'final_evaluation')
                                {{ date("H:i a", strtotime($data->timeslot->mid_year))}}
                            @endif
                        </td>
                        <td>{{$data->meeting_status}}</td>
                        <td>
                            <span>
                                @if ($data->meeting_status == 'no')
                                    <a href="javascript:;" class="meetingresponse" title="Check Status" data-id="{{ $data->id }}" data-group-id="{{ $data->group_id }}">
                                        <span class="svg-icon svg-icon-md">
                                            <i class="fas fa-edit text-primary mr-2"></i>
                                        </span>
                                    </a>
                                @else
                                    <span class="svg-icon svg-icon-md">
                                        <i class="fas fa-check-circle mr-2" style="color: #1dab42"></i>
                                    </span>
                                @endif
                            </span>
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
  <div class="modal fade" id="ApprovedModal" tabindex="-1" role="dialog" aria-labelledby="CommentModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Meeting Response</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="POST" action="{{ route('panelmeeting.store') }}">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col md-12">
                        <input type="hidden" name="id" id="repsonse_id" value=""/>
                        <input type="hidden" name="group_id" id="group_id" value=""/>
                        <textarea class="form-control" name="comment" required placeholder="Enter your feedback" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-light-success btn-sm btn-shadow font-weight-bolder font-size-sm float-right active">Submit &nbsp;<i class="fas fa-check-circle"></i></button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
@section('footer.scripts')
<script>

    $( "#kt_datatable" ).on('click', '.meetingresponse', function() {
        var id = $(this).data('id');
        var groupID = $(this).data('group-id');
        $('#repsonse_id').val(id);
        $('#group_id').val(groupID);
        $('#ApprovedModal').modal('toggle');
    });

    $("#schedulerfor").change(function(){
        var schedulerfor = $(this).children("option:selected").val();
        if(schedulerfor == 'mid_year' || schedulerfor == 'final_evaluation'){
            $('#teacherdiv').show();
        }else{
            $('#teacherdiv').hide();
        }
    });

    $( function() {
      $( "#datepicker" ).datepicker({
          minDate: 0,
            beforeShowDay: function(date) {
            var show = true;
            if(date.getDay()==5 || date.getDay()==6 || date.getDay()==0) show=false
            return [show];
            }
        });
    } );
    </script>
@endsection
