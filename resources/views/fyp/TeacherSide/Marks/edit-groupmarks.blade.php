@extends('fyp.layouts.app')
@section('content')
<div class="d-flex flex-column" id="kt_wrapper">

    @extends('fyp.layouts.head')

    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::List Widget 2-->
                        <div class="card card-custom bg-light-success card-stretch gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-6 mb-2">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label font-weight-bold font-size-h4 text-dark-75 mb-3">{{ $groupdata->title}}</span>
                                    <span class="text-muted font-weight-bold font-size-sm">{{ $groupdata->description}}</span>
                                </h3>
                                {{-- <div class="card-toolbar">
                                    <button class="btn btn-light-primary btn-sm btn-shadow font-weight-bolder font-size-sm" data-toggle="modal" data-target="#projectporposal">
                                        Project Proposal &nbsp;<i class="fas fa-eye"></i>
                                    </button>
                                    &nbsp;
                                    &nbsp;
                                    <a href="{{ route('download.proposal', $groupdata->id) }}" class="btn btn-light-warning btn-sm btn-shadow font-weight-bolder font-size-sm">
                                        Project Proposal &nbsp;<i class="fas fa-file-download"></i>
                                    </a>
                                </div> --}}
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2">
                                @foreach ($students as $student)

                                <div class="card-toolbar">
                                    <a href="{{route('marks.edit',['groupid' =>$groupdata->id,'studentid'=>$student->id])}}" class="btn btn-success btn-sm btn-shadow font-weight-bolder font-size-sm float-right active">
                                        Add Marks &nbsp;<i class="fas fa-check-circle"></i></a>
                                    </div>

                                <!--begin::Item-->
                                <div class="d-flex align-items-center mb-10">
                                    <!--begin::Symbol-->
                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                        <div class="symbol-label">
                                            <img src="{{ URL:: asset('fyp/assets/media/svg/avatars/004-boy-1.svg')}}" class="h-75 align-self-end" alt="" />
                                        </div>
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Text-->
                                    <div class="d-flex flex-column font-weight-bold">
                                        <span class="text-dark text-hover-primary mb-2 font-size-lg">{{ $student->name}}
                                        </span>
                                        <span class="text-muted">{{$student->roll_no}}</span>
                                    </div>
                                    <!--end::Text-->
                                </div>
                                <!--end::Item-->
                                @endforeach
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 2-->
                    </div>
                </div>
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
</div>

{{-- PDF Modal --}}
<!-- Modal-->
<div class="modal fade" id="projectporposal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Project Propsal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <iframe src ="{{ asset($groupdata->path)}}" width="100%" height="500px"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection
