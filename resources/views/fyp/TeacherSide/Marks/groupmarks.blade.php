@extends('fyp.layouts.app')
@section('content')
<div class="row">
    @foreach ($groups as $index=> $data)
    <div class="col-xl-6">
        <!--begin::Stats Widget 1-->
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b"
            style="background-position: right top; background-size: 30% auto; background-image: url(fyp/assets/media/svg/shapes/abstract-{{++$index}}.svg)">
            <!--begin::Body-->
            <div class="card-body">
                <a href="{{ route('add.marks', ['id' => $data->id])}}" class="card-title font-weight-bold text-muted text-hover-primary font-size-h5">{{$data->title}}</a>
                <div class="font-weight-bold text-success mt-9 mb-5">
                    @if ($data->feedback)
                        <a href="javascript:void(0)" onclick="groupdetials({{$data->id}})" class="btn btn-light-primary">Panel Feedback</a>
                    @else
                        <span class="p-4 badge badge-primary" style="font-size: 13px;">No Feeback Yet</span>
                    @endif
                </div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">{{$data->description}}</p>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 1-->
    </div>
    @endforeach
</div>
<!-- Modal-->
<div class="modal fade" id="groupdetials" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Group Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::List Widget 2-->
                        <div class="card card-custom bg-light-success">
                            <!--begin::Body-->
                            <div class="card-body pt-10">
                                <!--begin::Item-->
                                <div class="" id="student">

                                </div>
                                <!--end::Item-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 2-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer.scripts')
<script>

    function groupdetials(id) {
        $.get('groupfeedback/'+id, function (data) {
            var feedback = data;
            console.log(data);
            $("#student").empty();

            var my_orders = $("#student");


            $.each(feedback, function(i,comments){

                my_orders.append('<div class="d-flex align-items-center mb-10"><div class="d-flex flex-column font-weight-bold"><span class="text-dark text-hover-primary mb-1 font-size-lg">'+comments.teacher.name+'</span><span class="text-muted">'+comments.feedback+'</span></div></div>');

            });
            $("#groupdetials").modal('toggle');

        });
    }

</script>
@endsection
