@extends('fyp.layouts.app')
@section('content')
    <div class="d-flex flex-column" id="kt_wrapper">

        @extends('fyp.layouts.head')

        <!--begin::Content-->
        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
            <!--begin::Entry-->
            <div class="d-flex flex-column-fluid">
                <!--begin::Container-->
                <div class="container">
                    <!--begin::Dashboard-->
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::List Widget 2-->
                            <div class="card card-custom bg-light-success card-stretch gutter-b">
                                <!--begin::Header-->
                                <div class="card-header border-0 pt-6 mb-2">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span
                                            class="card-label font-weight-bold font-size-h4 text-dark-75 mb-3">{{ $groupdata->title }}</span>
                                        <span
                                            class="text-muted font-weight-bold font-size-sm">{{ $groupdata->description }}</span>
                                    </h3>

                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body ">
                                    @foreach ($students as $student)

                                        <!--begin::Item-->
                                        <div class="d-flex align-items-center mb-10">
                                            <!--begin::Symbol-->
                                            <div class="symbol symbol-40 symbol-light-white mr-5">
                                                <div class="symbol-label">
                                                    <img src="{{ URL::asset('fyp/assets/media/svg/avatars/004-boy-1.svg') }}"
                                                        class="h-75 align-self-end" alt="" />
                                                </div>
                                            </div>
                                            <!--end::Symbol-->
                                            <!--begin::Text-->
                                            <div class="d-flex flex-column font-weight-bold">
                                                <span
                                                    class="text-dark text-hover-primary mb-2 font-size-lg">{{ $student->name }}
                                                </span>
                                                <span class="text-muted">{{ $student->roll_no }}</span>
                                            </div>
                                            <!--end::Text-->
                                        </div>
                                        <!--end::Item-->
                                    @endforeach
                                </div>

                                <form
                                    {{-- action="{{ route('add.marks.added', ['groupid' => $groupdata->id, 'studentid' => $students[0]->id]) }}" --}}
                                    method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="class p-10">
                                        <div class="form-group row ">
                                            <label class="col-3 col-form-label font-weight-bold">Mid Semester
                                                Evaluation</label>
                                            <div class="col-4">
                                                <input class="form-control @error('midevolution') is-invalid @enderror"
                                                    min="0" max="40" step="0.01" type="number" name="midevolution"

                                                    {{-- logic here start --}}
                                                    @if($stdmarks != null)
                                                    @if($stdmarks->islocked_mid_marks == 1)
                                                    readonly
                                                    value="{{old('midevolution',$stdmarks->mid_semester_evaluation)}}"
                                                    @else value="{{old('midevolution',$stdmarks->mid_semester_evaluation)}}"
                                                    @endif
                                                    @else value=""
                                                    @endif
                                                    {{-- logic here end --}}

                                                    id="example-text-input" placeholder="Enter the marks" required />
                                                @error('midevolution')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            {{-- logic here start --}}
                                            @if($stdmarks == null || $stdmarks->islocked_mid_marks == 0)
                                            <div class="col-5 mt-3">
                                                <div class="form-group">
                                                    <input type="checkbox" name="islocked_mid_marks" id="status1" @if($stdmarks !=  null) {{($stdmarks->islocked_mid_marks == 1) ?'checked': ''}} @else @endif data-toggle="toggle" data-on="Enabled"
                                                        data-off="Disabled">
                                                    <label for="status1" class="font-weight-bold">Locked</label>
                                                    @error('islocked_mid_marks')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            @else
                                            @endif
                                            {{-- logic here end --}}

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label font-weight-bold">Mid year Evaluation</label>
                                            <div class="col-4">
                                                <input class="form-control @error('midyearevolution') is-invalid @enderror"
                                                    min="0" max="60" step="0.01" type="number" name="midyearevolution"
                                                    id="example-text-input" placeholder="Enter the marks"

                                                    {{-- logic here start --}}
                                                    @if($stdmarks != null)
                                                    @if($stdmarks->islocked_midyear_marks == 1)
                                                    readonly
                                                    value="{{old('midyearevolution',$stdmarks->mid_year_evaluation)}}"
                                                    @else value="{{old('midyearevolution',$stdmarks->mid_year_evaluation)}}"
                                                    @endif
                                                    @else value=""
                                                    @endif
                                                    {{-- logic here end --}}

                                                    required />
                                                @error('midyearevolution')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            {{-- logic here start --}}
                                            @if($stdmarks == null || $stdmarks->islocked_midyear_marks == 0)
                                            <div class="col-5 mt-3">
                                                <div class="form-group">
                                                    <input type="checkbox" name="islocked_midyear_marks" id="status2" @if($stdmarks !=  null) {{($stdmarks->islocked_midyear_marks == 1) ?'checked': ''}} @else @endif data-toggle="toggle" data-on="Enabled"
                                                        data-off="Disabled">
                                                    <label for="status2" class="font-weight-bold">Locked</label>
                                                    @error('islocked_midyear_marks')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            @else
                                            @endif
                                            {{-- logic here end --}}


                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label font-weight-bold">Final year
                                                Evaluation</label>
                                            <div class="col-4">
                                                <input
                                                    class="form-control @error('finalyearevolution') is-invalid @enderror"
                                                    min="0" max="100" step="0.01" type="number" name="finalyearevolution"
                                                    id="example-text-input" placeholder="Enter the marks"

                                                    {{-- logic here start --}}
                                                    @if($stdmarks != null)
                                                    @if($stdmarks->islocked_finalyear_marks == 1)
                                                    readonly
                                                    value="{{old('finalyearevolution',$stdmarks->final_year_evaluation)}}"
                                                    @else value="{{old('finalyearevolution',$stdmarks->final_year_evaluation)}}"
                                                    @endif
                                                    @else value=""
                                                    @endif
                                                    {{-- logic here end --}}

                                                    required />
                                                @error('finalyearevolution')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            {{-- logic here start --}}
                                            @if($stdmarks == null || $stdmarks->islocked_finalyear_marks == 0)
                                            <div class="col-5 mt-3">
                                                <div class="form-group">
                                                    <input type="checkbox" name="islocked_finalyear_marks" id="status3" @if($stdmarks !=  null) {{($stdmarks->islocked_finalyear_marks == 1) ?'checked': ''}} @else @endif data-toggle="toggle" data-on="Enabled"
                                                        data-off="Disabled">
                                                    <label for="status3" class="font-weight-bold">Locked</label>
                                                    @error('islocked_finalyear_marks')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            @else
                                            @endif
                                            {{-- logic here end --}}

                                        </div>
                                        <div class="card-toolbar">
                                            <button type="submit"
                                                class="btn btn-success btn-sm btn-shadow font-weight-bolder font-size-sm float-right active">
                                                Submit Marks &nbsp;<i class="fas fa-check-circle"></i></button>
                                        </div>
                                    </div>
                                    <form>
                                        <!--end::Body-->
                            </div>
                            <!--end::List Widget 2-->
                        </div>
                    </div>
                    <!--end::Dashboard-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
        <!--end::Content-->
    </div>

    {{-- PDF Modal --}}
    <!-- Modal-->
    <div class="modal fade" id="projectporposal" data-backdrop="static" tabindex="-1" role="dialog"
        aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Project Porposal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <iframe src="{{ asset($groupdata->path)}}" width="100%" height="500px"></iframe> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection
