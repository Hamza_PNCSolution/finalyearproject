@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Change Password
        </h3>
    </div>
    <!--begin::Form-->
    <div class="panel panel-default">
        @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif
    </div>
    <form action="{{ route('update.password') }}" method="POST" enctype="multipart/form-data">
        @csrf

        @foreach ($errors->all() as $error)
        <div class="row pt-2 pl-2 pr-2">
            <div class="col-lg-12">
                <div class="alert-danger rounded" role="alert">
                    <p class="p-2">{{ $error }}</p>
                </div>
            </div>
        </div>
        @endforeach
        <div class="card-body">

            <div class="form-group row">
                <label class="col-3 col-form-label">Current Password</label>
                <div class="col-9">
                    <div class="input-group">
                        <input type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" id="old_password">
                        <div class="input-group-append">
                            <span class="input-group-text" onclick="editUserInfo(this)" style="background-color: transparent; cursor: pointer;">
                                <i class="fa fa-eye"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3 col-form-label">New Password</label>
                <div class="col-9">
                    <div class="input-group">
                        <input type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" id="new_password">
                        <div class="input-group-append">
                            <span class="input-group-text" onclick="editUserInfo(this)" style="background-color: transparent; cursor: pointer;">
                                <i class="fa fa-eye"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3 col-form-label">New Confroms Password</label>
                <div class="col-9">
                    <div class="input-group">
                        <input type="password" class="form-control @error('conform_password') is-invalid @enderror" name="conform_password" id="conform_password">
                        <div class="input-group-append">
                            <span class="input-group-text" onclick="editUserInfo(this)" style="background-color: transparent; cursor: pointer;">
                                <i class="fa fa-eye"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Update</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('footer.scripts')
<script>
function editUserInfo(el) {
  input_element = $(el).parents('.input-group').find('input.form-control');
  console.log(input_element.attr('id'));
  console.log(input_element.val());
  if(input_element.attr('id') == 'old_password'){
    $('#old_password').removeAttr("type");
    $('#old_password').prop('type', 'text');

  }if(input_element.attr('id') == 'new_password'){
    $('#new_password').removeAttr("type");
    $('#new_password').prop('type', 'text');

  }if(input_element.attr('id') == 'conform_password'){
    $('#conform_password').removeAttr("type");
    $('#conform_password').prop('type', 'text');
  }
}
</script>
@endsection
