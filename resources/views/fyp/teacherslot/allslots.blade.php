@extends('fyp.layouts.app')
@section('content')
<div class="row">
    @foreach ($slots as $index=> $data)
    <div class="col-xl-6">
        <!--begin::Stats Widget 1-->
        <div class="card card-custom bgi-no-repeat card-stretch gutter-b"
            style="background-position: right top; background-size: 30% auto; background-image: url(fyp/assets/media/svg/shapes/abstract-{{++$index}}.svg)">
            <!--begin::Body-->
            <div class="card-body">
                <span class="card-title font-weight-bold text-primary font-size-h5">{{$data->user->name}}</span>
                <div class="font-weight-bold text-success mt-9 mb-5">{{date('h:i a ', strtotime($data->start_time))}} - {{date('h:i a ', strtotime($data->end_time))}}</div>
                <p class="text-dark-75 font-weight-bolder font-size-h5 m-0">{{ $data->room }}</p>
            </div>
            <!--end::Body-->
        </div>
        <!--end::Stats Widget 1-->
    </div>
    @endforeach
</div>
@endsection
@section('footer.script')
<script>
</script>
@endsection
