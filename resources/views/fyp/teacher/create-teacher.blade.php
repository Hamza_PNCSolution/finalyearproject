@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            New Teacher
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('teacher.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">

            <div class="form-group row">
                <label class="col-2 col-form-label">Name</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="name" id="example-text-input" placeholder="Enter name" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Cloud ID</label>
                <div class="col-10">
                    <input class="form-control" type="email" name="email" id="example-text-input" placeholder="Enter email" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Phone No</label>
                <div class="col-10">
                    <input class="form-control" type="tel" name="phone" id="example-text-input" placeholder="Enter phone" required/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Post</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="post" id="example-text-input" placeholder="Enter post" required/>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Domain</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="domain" id="example-text-input" placeholder="Enter domain" required/>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Save</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
