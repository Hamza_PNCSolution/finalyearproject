@extends('fyp.layouts.app')
@section('style ')
<style>
span.select2.select2-container.select2-container--default.select2-container--focus {
    width: 100%;
}
</style>
@endsection
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Committee</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <div class="dropdown dropdown-inline mr-2">
                <button type="submit" class="btn btn-light-primary font-weight-bolder"  data-toggle="modal" data-target="#NewMember">
                    <span class="svg-icon svg-icon-md">
                        <i class="fas fa-plus-circle"></i>
                    </span>New Memeber</button>
            </div>
            <!--end::Dropdown-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th >Sr No</th>
                    <th title="Field #4">Name</th>
                    <th title="Field #5">Cloud ID</th>
                    <th title="Field #4">Phone</th>
                    <th title="Field #2">Post</th>
                    <th title="Field #4">Domain</th>
                    <th title="Field #6">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $x = 0  ?>
                @foreach ($committee as $data)
                <tr>
                    <td >{{$x+1}}</td>
                    <td>{{$data->name}}</td>
                    <td class="text-right">{{$data->email}}</td>
                    <td class="text-right">{{$data->phone}}</td>
                    <td class="text-right">{{$data->post}}</td>
                    <td class="text-right">{{$data->domain}}</td>
                    <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                        <span style="overflow: visible; position: relative; width: 125px;">
                        <a href="{{ route('teacher.edit',$data->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            {{-- <form method="Post" action="{{ route('teacher.destroy',$data->id) }}" style="display: -webkit-inline-box;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                    <span class="svg-icon svg-icon-md">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                </button>
                            </form> --}}
                        </span>
                    </td>
                </tr>
                <?php $x++;?>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
  <!-- Modal -->
  <div class="modal fade" id="NewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">New Committee Member</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('store.committee') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-lg-12">
                        <select name="members[]" id="members" class="form-control form-control-solid select2" multiple="multiple" style="width: 100% !important;" required>
                            @foreach ($teachers as $teacher)
                                <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('footer.scripts')
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endsection
