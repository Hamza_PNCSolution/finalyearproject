@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">List of All Groups
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th>Sr No</th>
                    <th>Name</th>
                    <th>Teacher</th>
                    <th>Group Status</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($groupdata as $key => $data)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$data->title}}</td>
                    <td>{{$data->group->teacher->name ?? '' }}</td>
                    <td>
                        <span class="@if ($data->is_approved != '0')
                            text-success font-weight-bolder
                         @elseif($data->is_rejected != '0')
                            text-danger font-weight-bolder
                         @else
                             text-warning font-weight-bolder
                         @endif">
                         @if ($data->is_approved != '0')
                            {{ 'Approved' }}
                         @elseif($data->is_rejected != '0')
                            {{ 'Rejected' }}
                         @else
                             {{ 'Pending' }}
                         @endif
                        </span>
                    </td>
                    <td>
                        <span style="overflow: visible; position: relative; width: 125px;">
                            <a href="{{ route('detials.groups', $data->id) }}" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-eye btn btn-primary mr-2"></i>
                                </span>
                            </a>
                        </span>
                    </td>

                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
