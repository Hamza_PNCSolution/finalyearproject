@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Groups Details
        </h3>
    </div>
    <!--begin::Form-->
    <div class="card-body">
        <div class="form-group row">
            <label class="col-2 col-form-label font-weight-bolder">Group Title</label>
            <div class="col-10">
                <label class="col-form-label">{{$groupdata->title}}</label>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-2 col-form-label font-weight-bolder">Group Members</label>
            <div class="col-10">
                @foreach ($students as $key => $student )
                <label class="col-form-label">{{ ++$key.'. '.$student->name }}</label><br>
                @endforeach
            </div>
        </div>

        <div class="form-group row">
            <label class="col-2 col-form-label font-weight-bolder">Group Teacher</label>
            <div class="col-10">
                <label class="col-form-label">{{$groupdata->group->teacher->name ?? 'No Teacher Assgined Yet' }}</label>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-2 col-form-label font-weight-bolder">Group Description</label>
            <div class="col-10">
                <label class="col-form-label">{{$groupdata->description}}</label>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-2 col-form-label font-weight-bolder">Group Proposal</label>
            <div class="col-10">
                <a href="{{$groupdata->path}}" target="_blank" class="btn btn-primary text-white">
                    <i class="fas fa-eye"></i> View
                </a>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-2 col-form-label font-weight-bolder">Group Status</label>
            <div class="col-10">
                <label class="col-form-label font-weight-bolder @if ($groupdata->is_approved == 1)
                        text-success
                    @elseif($groupdata->is_rejected == 1)
                        text-danger
                    @else
                        text-warning
                    @endif">
                    @if($groupdata->is_approved == 1)
                        {{ 'Approved'}}
                    @elseif ($groupdata->is_rejected == 1)
                        {{ 'Rejected' }}
                    @else
                        {{ 'Pending' }}
                    @endif
                </label>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-3 col-form-label font-weight-bolder">Group Comment By Teacher</label>
            <div class="col-9">
                <label class="col-form-label">{{$groupdata->comments->comment ?? 'No Comment Found'}}</label>
            </div>
        </div>

        @if ($groupdata->is_rejected == 1)
        <form action="{{ route('assign.groups') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label class="col-3 col-form-label font-weight-bolder">Group Assign To Teacher</label>
                <div class="col-9">
                    <input type="hidden" name="group_id" value="{{ $groupdata->id }}" />
                    <select class="form-control" name="teacher_id">
                        @foreach ($teachers as $teacher)
                        <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                        @endforeach
                    </select>
                    <br>
                    <button type="submit" class="btn btn-primary">Assign</button>
                </div>
            </div>
        </form>
        @endif
    </div>
</div>
@endsection
@section('footer.scripts')
<script>
</script>
@endsection
