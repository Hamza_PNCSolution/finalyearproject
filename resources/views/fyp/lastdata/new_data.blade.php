@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            New Group Data
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('store.lastfyp') }}" method="POST" enctype="multipart/form-data" class="repeater">
        @csrf
        <div class="card-body">


            <div class="form-group row">
                <label class="col-3 col-form-label">Project Document</label>
                <div class="col-9">
                    <input type="file" name="document[]" class="dropify" data-allowed-file-extensions="pdf doc"  data-max-file-size-preview="2000M"  data-show-loader="true" data-show-errors="true" multiple/>
                    @error('document')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Upload</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('footer.scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.1.2/jquery.repeater.min.js"></script>
<script>
        $('.repeater').repeater({
        isFirstItemUndeletable: true

    });
</script>
@endsection
