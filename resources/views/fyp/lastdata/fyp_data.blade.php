@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Previous Final Year Project Data
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a class="btn btn-secondary font-weight-bolder mr-4" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-search"></i>
                </span>Keyword
            </a>
            <!--end::Button-->
            @hasrole('Admin')
            <!--begin::Button-->
            <a href="{{ route('new.lastfyp') }}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Recored
            </a>
            <!--end::Button-->
            @endrole
        </div>
    </div>
    <div class="card-body collapse" id="collapseExample" style="padding: 2rem 2.25rem;margin-bottom: -35px;">
        <div class="mb-9">
            <div class="row align-items-center">
                <form method="GET" action="{{ route('keyword.search') }}" style="display: contents;">
                    <div class="col-lg-9">
                        <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" name="keyword" class="form-control" placeholder="Search..." required value="{{request()->keyword}}"/>
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-lg-3">
                        <button type="submit" class="btn btn-light-primary px-24 font-weight-bold">Search</button>
                    </div>
                </form>
            </div>
        </div>
        <hr>
    </div>
    <div class="card-body">
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Search..."
                                        id="kt_datatable_search_query" />
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <button type="submit" class="btn btn-light-primary px-6 font-weight-bold">Search</button>
                    </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th>Sr No</th>
                    <th>Filename</th>
                    <th>Document</th>
                    @hasrole('Admin')
                    <th>Action</th>
                    @endrole
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $data)
                {{-- @dd(json_decode($data->group_members)) --}}
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{ $data->filename}}</td>
                    <td>{!! isset($data->document) ? '<a href="storage/'.$data->document.'" target="_blank"  class="btn btn-primary"><i class="fas fa-eye"></i>View</a>' : 'No Upload' !!}</td>
                    @hasrole('Admin')
                    <td>
                        <span>
                            <a href="{{ route('delete.lastfyp', $data->id) }}" class="btn btn-danger" title="Delete"><i class="fas fa-trash pl-1"></i></a>
                        </span>
                    </td>
                    @endrole
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
`
