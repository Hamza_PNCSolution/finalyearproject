@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            @if (!$checkgroup)
                Student Group
            @else
                Proporsal Teachers Feedback
            @endif
        </h3>
    </div>
    <!--begin::Form-->
    {{-- @dd(now()->timezone('asia/karachi')->format('Y-m-d h:i A')) --}}
    {{-- @dd($converteddate) --}}
    @if (!$checkgroup)
        <form action="{{ route('add.group') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">

                <div class="form-group row">
                    <label class="col-2 col-form-label">Project Title</label>
                    <div class="col-10">
                        <input class="form-control  @error('title') is-invalid @enderror" type="title" id="" name="title" id="example-text-input" placeholder="Enter project title" />
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">Teacher</label>
                    <div class="col-10">
                        <select class="form-control select2 @error('teachers') is-invalid @enderror" id="teacherselect" name="teachers[]" multiple="multiple">
                            @foreach($teachers as $teacher)
                                <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                            @endforeach
                        </select>
                        @error('teachers')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-2 col-form-label">Students</label>
                    <div class="col-10">
                        <select class="form-control select2 @error('students') is-invalid @enderror" id="studentselect " name="students[]" multiple="multiple">
                            @foreach($students as $std)
                                <option value="{{$std->id}}">{{$std->name}}</option>
                            @endforeach
                            </select>
                            @error('students')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-search-input" class="col-2 col-form-label">Description</label>
                    <div class="col-10">
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="3"></textarea>
                        @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Project Proposal</label>
                    <div class="col-10">
                        <input type="file" name="proposal" class="dropify  @error('proposal') is-invalid @enderror" data-allowed-file-extensions="pdf xlsx"  data-max-file-size-preview="3M"
                        data-show-errors="true" />
                    </div>
                    @error('proposal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                </div>

            </div>
            <div class="card-footer">
                <div class="row">
                    {{-- @dd(now()->timezone('asia/karachi')->format('Y-m-d h:i A')) --}}
                @if ($convertedate ?? '' >= now()->timezone('asia/karachi')->format('Y-m-d h:i A'))
                    <div class="col-2">
                        <button type="submit" class="btn btn-success btn-lg btn-block">Save</button>
                    </div>
                    <div class="col-10">
                    </div>
                @endif
                </div>
            </div>
        </form>
    @else
    <div class="card-body">
        <div class="card-body">
            <!--begin: Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Search..."
                                        id="kt_datatable_search_query" />
                                    <span>
                                        <i class="flaticon2-search-1 text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--begin: Datatable-->
            <br>
            <div class="panel panel-default">
                @if (session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @elseif (session('delete'))
                    <div class="alert alert-danger">{{ session('delete') }}</div>
                @elseif (session('update'))
                    <div class="alert alert-success">{{ session('update') }}</div>
                @endif
            </div>
            <br>
            <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
                <thead>
                    <tr>
                        <th>Sr No</th>
                        <th>Teacher</th>
                        <th>Comment</th>
                        <th>Progress</th>
                        <th>Submission</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($comment as $index => $data)
                    <tr>
                        <td >{{++$index}}</td>
                        <td>{{$data->teacher->name}}</td>
                        <td>{{$data->comment}}</td>
                        <td><span class="@if($data->for == 'reject') text-danger font-weight-bold @else text-success font-weight-bold @endif">{{ucwords($data->for)}}</span></td>
                        <td>
                            @if (isset($data->proposal_id))
                                @if ($proposal->group->teacher_proposalweb)
                                    <span><p class="btn bg-success text-white">Proposal Submitted</p></span>
                                @else
                                <a herf="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#Resubmission">Resubmit Proposal</a>
                                @endif
                            @else
                                <span><p class="btn bg-success text-white">No Resubmission</p></span>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    @endif
</div>
{{-- @dd($checkgroup->stdgroup->group_id) --}}
<div class="modal fade" id="Resubmission" tabindex="-1" role="dialog" aria-labelledby="CommentModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Proposal Resubmission</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="POST" action="{{ route('up.proposal') }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="form-group row">
                    <label for="example-search-input" class="col-2 col-form-label">Title</label>
                    <div class="col-10">
                        <input class="form-control" type="title" name="title" value="{{$proposal->title ?? ''}}" readonly/>
                        <input type="hidden" name="group_id" value="{{ $checkgroup->stdgroup->group_id ?? '' }}" />
                        <input type="hidden" name="proposal_id" value="{{ $proposal->id ?? '' }}" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-search-input" class="col-2 col-form-label">Description</label>
                    <div class="col-10">
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="3" required placeholder="Enter the description"></textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Project Proposal</label>
                    <div class="col-10">
                        <input type="file" name="new_proposal" class="dropify  @error('proposal') is-invalid @enderror" data-allowed-file-extensions="pdf xlsx doc"  data-max-file-size-preview="3M"
                        data-show-errors="true" required/>
                    </div>
                    @error('proposal')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-light-success btn-sm btn-shadow font-weight-bolder font-size-sm float-right active">Submit &nbsp;<i class="fas fa-check-circle"></i></button>
            </div>
        </form>
        </div>
    </div>
</div>

@endsection
@section('footer.scripts')
<script>
    $(document).ready(function() {
            $('.select2').select2();

        // limiting the number of selections
        $('#teacherselect').select2({
         placeholder: "Select an option",
         maximumSelectionLength: 4
        });

        $('#studentselect').select2({
         placeholder: "Select an option",
         maximumSelectionLength: 4
        });

        });
</script>
@endsection
