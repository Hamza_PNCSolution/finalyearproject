@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Assgined Activity
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('store.stdactivity') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Activity Title</label>
                <div class="col-10">
                    <label class="col-2 col-form-label font-weight-bold">{{$activity->title}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Deadline Date</label>
                <div class="col-10">
                    <label class="col-2 col-form-label font-weight-bold">{{$activity->end_date}}</label>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Description</label>
                <div class="col-10">
                    <label class="col-2 col-form-label font-weight-bold">{{$activity->description}}</label>
                </div>
            </div>

            @if ($groupleader)
            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Submitted Date</label>
                <div class="col-10">
                    <input class="form-control" type="hidden" name="activity_id" value="{{$activity->id}}"/>
                    @if ($activity->status == 'Assigned')
                        <input class="form-control" type="date" name="submitted date" />
                    @else
                        <label class="col-2 col-form-label font-weight-bold">{{$activity->activitymeta->submitted_date}}</label>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label font-weight-bolder">Comment</label>
                <div class="col-10">
                    @if ($activity->status == 'Assigned')
                        <input type="text" name="comment" class="form-control" placeholder="Enter Any Comment"/>
                    @else
                        <label class="col-2 col-form-label font-weight-bold">{{$activity->activitymeta->comment}}</label>
                    @endif
                </div>
            </div>
            @endif

            @if ($activity->document == 'on')
                <div class="form-group row">
                    <label class="col-2 col-form-label font-weight-bolder">Activity Document</label>
                    <div class="col-10">
                        @if ($activity->status == 'Assigned')
                        <input type="file" name="document" class="dropify" data-allowed-file-extensions="pdf doc"  data-max-file-size-preview="3M"
                        data-show-errors="true" />
                    @else
                    <a href="{{ asset('storage/'.$activity->activitymeta->document)}}"  class="btn btn-light-primary btn-sm btn-shadow font-weight-bolder font-size-sm" target="_blank"> View Document &nbsp;<i class="fas fa-eye"></i></a>
                    @endif
                    </div>
                </div>
            @endif

        </div>

        @if ($groupleader)
            @if ($activity->status == 'Assigned')
            <div class="card-footer">
                <div class="row">
                    <div class="col-2">
                        <button type="submit" class="btn btn-success btn-lg btn-block">Save</button>
                    </div>
                    <div class="col-10">
                    </div>
                </div>
            </div>
            @endif
        @endif
    </form>
</div>
@endsection
@section('footer.scripts')
<script>
</script>
@endsection
