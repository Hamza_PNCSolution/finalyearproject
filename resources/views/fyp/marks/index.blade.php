@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Marks Details</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            {{-- <a href="{{ route('student.create') }}" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#SlotModal">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Slot</a> --}}
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th >Sr No</th>
                    <th>Student Name</th>
                    <th>Teacher Name</th>
                    <th>Group Title</th>
                    <th>Mid Semester</th>
                    <th>Mid Year</th>
                    <th>Final Year</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($marks as $index => $data)
                <tr>
                    <td >{{++$index}}</td>
                    <td>{{ $data->student->name }}</td>
                    <td>{{ $data->teacher->name }}</td>
                    <td>{{ $data->groups->title }}</td>
                    <td>{{ ($data->islocked_mid_marks == 1) ? $data->mid_semester_evaluation : 'Not Assign'}}</td>
                    <td class="text-right">{{ ($data->islocked_midyear_marks == 1) ? $data->mid_year_evaluation : 'Not Assign'}}</td>
                    <td class="text-right">{{ ($data->islocked_finalyear_marks == 1) ? $data->final_year_evaluation : 'Not Assign'}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
