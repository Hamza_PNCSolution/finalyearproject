@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Group Scheduler
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            @if (auth()->user()->hasrole('Admin'))
                <div class="dropdown dropdown-inline mr-2">
                    <button type="submit" class="btn btn-light-primary font-weight-bolder btn-generate"  data-toggle="modal" data-target="#NewMember">Generate</button>
                </div>
            @endif
            <!--end::Dropdown-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="table table-separate table-responsive table-head-custom table-checkable table-sm table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Groups</th>
                    <th>Meeting Type</th>
                    <th>Meeting Panel</th>
                    <th>Rooms</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Date</th>
                    <th>Meeting Response</th>
                    <th>Meeting Feedback</th>
                    <th>Meeting Status</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($scheduler))
                    @php
                        $count = $teachercount / 2;
                    @endphp
                    @foreach ($scheduler as $key => $data)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$data->group->title}}</td>
                        <td>{{$data->meeting_type}}</td>
                        <td>
                            @if (count($data->panelteacher) != 0)
                                @if ($key < 18)
                                    @if ($key < 9)
                                        @for ($i = 0; $i < $count; $i++)
                                            {!!$data->panelteacher[$i]->teacher->name.','.'</br>'!!}
                                        @endfor
                                    @else
                                        @for ($i = $count; $i < $teachercount; $i++)
                                            {!!$data->panelteacher[$i]->teacher->name.','.'</br>'!!}
                                        @endfor
                                    @endif
                                @endif
                            @elseif($data->meeting_type == 'mid_semester')
                                {{ $data->group->group->teacher->name }}
                            @else
                                {{ 'Committee Members' }}
                            @endif
                        </td>
                        <td>{{$data->room}}</td>
                        <td>
                            @if ($data->meeting_type == 'defense_one' || $data->meeting_type == 'defense_two')
                                {{ date("H:i a", strtotime($data->timeslot->defense))}}
                            @elseif($data->meeting_type == 'mid_semester')
                                {{ date("H:i a", strtotime($data->timeslot->mid_semester))}}
                            @elseif($data->meeting_type == 'mid_year' || $data->meeting_type == 'final_evaluation')
                                {{ date("H:i a", strtotime($data->timeslot->mid_year))}}
                            @endif
                        </td>
                        <td>
                            @if ($data->meeting_type == 'defense_one' || $data->meeting_type == 'defense_two')
                                {{ Carbon\Carbon::parse($data->timeslot->defense)->addminutes(15)->format('H:i a') }}
                            @elseif($data->meeting_type == 'mid_semester')
                                {{ Carbon\Carbon::parse($data->timeslot->mid_semester)->addminutes(20)->format('H:i a') }}
                            @elseif($data->meeting_type == 'mid_year' || $data->meeting_type == 'final_evaluation')
                                {{ Carbon\Carbon::parse($data->timeslot->mid_year)->addminutes(30)->format('H:i a') }}
                            @endif
                        </td>
                        <td>{{$data->meeting_date}}</td>
                        <td>{{ ($data->meeting_response) ? ucfirst($data->meeting_response) : 'Pending' }}</td>
                        <td>{{$data->feedback ?? 'Pending'}}</td>
                        <td>{{$data->meeting_status}}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
  <!-- Modal -->
  <div class="modal fade" id="NewMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">New Schedule Generate</h5>
        </div>
        <form action="{{ route('store.scheduler') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-3 col-form-label">Scheduler For</label>
                    <div class="col-lg-9">
                        <select name="for" id="schedulerfor" class="form-control form-control-solids" style="width: 100% !important;" required>
                            <option value="">Please Select Type of Scheduler</option>
                            <option value="defense_one">Defense One</option>
                            <option value="defense_two">Defense Two</option>
                            <option value="mid_semester">Mid semester</option>
                            <option value="mid_year">Mid year</option>
                            <option value="final_evaluation">Final evaluation</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Start Date</label>
                    <div class="col-lg-9">
                        <input type="text" id="datepicker" name="startdate" class="form-control" autocomplete="off" placeholder="Enter the start date"  required/>
                    </div>
                </div>
                <div class="field_wrapper">
                    <div class="form-group row">
                        <label class="col-3 col-form-label">Room 1</label>
                        <div class="col-lg-9">
                            <input type="text" name="room[]" class="form-control" autocomplete="off" placeholder="Room No."  required/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-3 col-form-label">Room 2</label>
                        <div class="col-lg-9">
                            <input type="text" name="room[]" class="form-control" autocomplete="off" placeholder="Room No."  required/>
                        </div>
                    </div>

                </div>
                <div class="form-group row" id="teacherdiv" style="display: none;">
                    <label class="col-3 col-form-label">Teacher</label>
                    <div class="col-lg-9">
                        <select name="teachers[]" id="teacher" class="form-control" multiple="multiple" style="width: 100% !important;">
                            @foreach ($teachers as $teacher)
                                <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                            @endforeach
                        </select>
                        <br>
                        <span class="p-5 text-danger font-weight-bold">Please select minimum two teacher for panel</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('footer.scripts')
<script>

    // For Teacher Selection
    $(document).ready(function() {
        $('#teacher').select2({
            placeholder: "Select the teachers",
            maximumSelectionLength : 6,
            allowClear: true
        });

        var answer = [];
        $("#kt_datatable tbody tr").each(function(i,e){
            answer.push($(e).find('td:last-child span').text());
        });
        console.log(answer);

        var found = answer.find(function (element) {
        return element === 'no';
    });
        if(found == 'no') {
            $('.btn-generate').hide();
        }
    });

    // For Teacher
    $( "#kt_datatable" ).on('click', '.meetingresponse2', function() {
        var id = $(this).data('id');
        var groupID = $(this).data('group-id-2');
        $('#repsonse_id2').val(id);
        $('#group_id2').val(groupID);
        $('#ApprovedModal2').modal('toggle');
    });

    $( "#kt_datatable" ).on('click', '.meetingresponse', function() {
        var id = $(this).data('id');
        var groupID = $(this).data('group-id');
        $('#repsonse_id').val(id);
        $('#group_id').val(groupID);
        $('#ApprovedModal').modal('toggle');
    });

    $("#schedulerfor").change(function(){
        var schedulerfor = $(this).children("option:selected").val();
        if(schedulerfor == 'mid_year' || schedulerfor == 'final_evaluation'){
            $('#teacherdiv').show();
        }else{
            $('#teacherdiv').hide();
        }
    });

    $( function() {
      $( "#datepicker" ).datepicker({
          minDate: 0,
            beforeShowDay: function(date) {
            var show = true;
            if(date.getDay()==5 || date.getDay()==6 || date.getDay()==0) show=false
            return [show];
            }
        });
    } );

    // For fields
    $(document).ready(function(){
    var maxField = 1; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper

    //Once add button is clicked
    $(addButton).click(function(){
        maxField++;
        //Check maximum number of input fields
        if(maxField < 3){

            var fieldHTML = '<div class="form-group row"><label class="col-3 col-form-label room_label_count">Room  '+maxField+'</label><div class="col-lg-7"><input type="text" name="room[]" class="form-control" autocomplete="off" placeholder="Room No."  required value=""/></div><div class="col-lg-2"><a href="javascript:void(0);" class="remove_button"><i class="fas fa-times-circle text-danger pt-3"></i></a></div></div>'; //New input field html

            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').parent('.row').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
    </script>
@endsection
