@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Group Scheduler
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            @if (auth()->user()->hasrole('Admin'))
                <div class="dropdown dropdown-inline mr-2">
                    <button type="submit" class="btn btn-light-primary font-weight-bolder btn-generate"  data-toggle="modal" data-target="#NewMember">Generate</button>
                </div>
            @endif
            <!--end::Dropdown-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <table class="table table-separate table-responsive table-head-custom table-checkable table-sm table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Groups</th>
                    <th>Meeting Type</th>
                    <th>Meeting Panel</th>
                    <th>Rooms</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Date</th>
                    <th>Meeting Status</th>
                    <th>Meeting Response</th>
                    <th>Meeting Feedback</th>
                </tr>
            </thead>
            <tbody>
                @if (isset($scheduler))
                    @php
                        $count = $teachercount / 2;
                    @endphp
                    @foreach ($scheduler as $key => $data)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$data->group->title}}</td>
                        <td>{{$data->meeting_type}}</td>
                        <td>
                            @if (count($data->panelteacher) != 0)
                                @if ($key < 18)
                                    @if ($key < 9)
                                        @for ($i = 0; $i < $count; $i++)
                                            {!!$data->panelteacher[$i]->teacher->name.','.'</br>'!!}
                                        @endfor
                                    @else
                                        @for ($i = $count; $i < $teachercount; $i++)
                                            {!!$data->panelteacher[$i]->teacher->name.','.'</br>'!!}
                                        @endfor
                                    @endif
                                @endif
                            @elseif($data->meeting_type == 'mid_semester')
                                {{ $data->group->group->teacher->name }}
                            @else
                                {{ 'Committee Members' }}
                            @endif
                        </td>
                        <td>{{$data->room}}</td>
                        <td>
                            @if ($data->meeting_type == 'defense_one' || $data->meeting_type == 'defense_two')
                                {{ date("H:i a", strtotime($data->timeslot->defense))}}
                            @elseif($data->meeting_type == 'mid_semester')
                                {{ date("H:i a", strtotime($data->timeslot->mid_semester))}}
                            @elseif($data->meeting_type == 'mid_year' || $data->meeting_type == 'final_evaluation')
                                {{ date("H:i a", strtotime($data->timeslot->mid_year))}}
                            @endif
                        </td>
                        <td>
                            @if ($data->meeting_type == 'defense_one' || $data->meeting_type == 'defense_two')
                                {{ Carbon\Carbon::parse($data->timeslot->defense)->addminutes(15)->format('H:i a') }}
                            @elseif($data->meeting_type == 'mid_semester')
                                {{ Carbon\Carbon::parse($data->timeslot->mid_semester)->addminutes(20)->format('H:i a') }}
                            @elseif($data->meeting_type == 'mid_year' || $data->meeting_type == 'final_evaluation')
                                {{ Carbon\Carbon::parse($data->timeslot->mid_year)->addminutes(30)->format('H:i a') }}
                            @endif
                        </td>
                        <td>{{$data->meeting_date}}</td>
                        <td>{{$data->meeting_status}}</td>
                        <td>{{$data->meeting_response ?? 'Pending'}}</td>
                        <td>{{$data->feedback ?? 'Pending'}}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
@endsection
@section('footer.scripts')
<script>
</script>
@endsection
