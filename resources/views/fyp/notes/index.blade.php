@extends('fyp.layouts.app')
@section('content')
<!--begin::Entry-->
<div class="panel panel-default">
    @if (session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @elseif (session('delete'))
        <div class="alert alert-danger">{{ session('delete') }}</div>
    @elseif (session('update'))
        <div class="alert alert-success">{{ session('update') }}</div>
    @endif
</div>
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <!--begin::Card-->
        <div class="card card-custom card-sticky" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Sticky Notes
                    </h3>
                </div>
                <div class="card-toolbar">
                    <div class="btn-group">
                        <form class="form" id="kt_form" method="POST" action="{{ route('note.store') }}">
                            @csrf
                        <button type="submit" class="btn btn-primary font-weight-bolder">
                            <i class="ki ki-check icon-xs"></i>Save Note</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <!--begin::Form-->
                    <div class="row">
                        <div class="col-xl-2"></div>
                        <div class="col-xl-8">
                            <div class="my-5">
                                <div class="form-group row">
                                    <label class="col-2">Title</label>
                                    <div class="col-10">
                                        <input class="form-control" name="title" type="text"
                                            placeholder="Enter the title of note" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2">Note</label>
                                    <div class="col-10">
                                        <textarea class="form-control" name="note" type="text"
                                            placeholder="Enter the title of note" rows="5" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid pt-7">
    <!--begin::Container-->
    <div class="container">
        <!--begin::Example-->
        <!--begin::Row-->
        <div class="row">
            {{-- @dd($note->count()) --}}
            @if ($note->count() != 0)
                @foreach ($note as $data)
                    <div class="col-lg-6 pt-2">
                        <!--begin::Example-->
                        <!--begin::Card-->
                        <div class="card card-custom" data-card="true" id="kt_card_3">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">{{ $data->title }}</h3>
                                </div>
                                <div class="card-toolbar">
                                    <a href="#" class="btn btn-icon btn-circle btn-sm btn-light-primary mr-1"
                                        data-card-tool="toggle">
                                        <i class="ki ki-arrow-down icon-nm"></i>
                                    </a>
                                    <form action="{{ route('note.destroy', $data->id) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button class="btn btn-icon btn-circle btn-sm btn-light-danger">
                                            <i class="ki ki-close icon-nm"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <div class="card-body">
                                <p>{{ $data->note }}</p>
                            </div>
                        </div>
                        <!--end::Card-->
                        <!--end::Example-->
                    </div>
                @endforeach
            @else
            <div class="col-lg-6">
                <!--begin::Example-->
                <!--begin::Card-->
                <div class="card card-custom" data-card="true" id="kt_card_3">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Title</h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-icon btn-circle btn-sm btn-light-primary mr-1"
                                data-card-tool="toggle">
                                <i class="ki ki-arrow-down icon-nm"></i>
                            </a>
                            <button class="btn btn-icon btn-circle btn-sm btn-light-danger">
                                <i class="ki ki-close icon-nm"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has ..</p>
                    </div>
                </div>
                <!--end::Card-->
                <!--end::Example-->
            </div>
            @endif
        </div>
        <!--end::Row-->
        <!--end::Example-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
@endsection
