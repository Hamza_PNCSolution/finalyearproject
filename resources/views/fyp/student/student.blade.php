@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Student</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <div class="dropdown dropdown-inline mr-2">
                <button type="submit" class="btn btn-light-primary font-weight-bolder"  data-toggle="modal" data-target="#ImportModal">
                    <span class="svg-icon svg-icon-md">
                        <i class="fas fa-file-upload"></i>
                    </span>Import</button>
            </div>
            <!--end::Dropdown-->
            <!--begin::Button-->
            <a href="{{ route('student.create') }}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <i class="fa fa-plus-circle"></i>
                </span>New Record</a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..."
                                    id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="panel panel-default">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @elseif (session('delete'))
                <div class="alert alert-danger">{{ session('delete') }}</div>
            @elseif (session('update'))
                <div class="alert alert-success">{{ session('update') }}</div>
            @endif
        </div>
        <br>
        <table class="datatable datatable-bordered datatable-head-custom table-striped" id="kt_datatable">
            <thead>
                <tr>
                    <th >Sr No</th>
                    <th title="Field #4">Roll No</th>
                    <th title="Field #4">Enrollment NO</th>
                    <th title="Field #5">Name</th>
                    <th title="Field #4">Phone</th>
                    <th title="Field #2">CGPA</th>
                    <th title="Field #4">Cloud ID</th>
                    <th title="Field #6">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student as $index => $data)
                <tr>
                    <td >{{++$index}}</td>
                    <td>{{$data->roll_no}}</td>
                    <td>{{$data->enrollment_no}}</td>
                    <td class="text-right">{{$data->name}}</td>
                    <td class="text-right">{{$data->phone}}</td>
                    <td class="text-right">{{$data->cgpa}}</td>
                    <td class="text-right">{{$data->email}}</td>
                    <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell">
                        <span style="overflow: visible; position: relative; width: 125px;">
                        <a href="{{ route('student.edit',$data->id) }}" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">
                                <span class="svg-icon svg-icon-md">
                                    <i class="fas fa-pen"></i>
                                </span>
                            </a>
                            {{-- <form method="Post" action="{{ route('student.destroy',$data->id) }}" style="display: -webkit-inline-box;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-clean btn-icon" title="Delete">
                                    <span class="svg-icon svg-icon-md">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                </button>
                            </form> --}}
                        </span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
  <!-- Modal -->
  <div class="modal fade" id="ImportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Upload File</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('import.student') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label>Student File</label>
                    <input type="file" name="file" class="form-control form-control-solid" style="height:50px;"/>
                   </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Upload</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
