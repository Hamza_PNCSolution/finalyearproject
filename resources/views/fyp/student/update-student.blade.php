@extends('fyp.layouts.app')
@section('content')
<div class="card card-custom">
    <div class="card-header">
        <h3 class="card-title">
            Update Student
        </h3>
    </div>
    <!--begin::Form-->
    <form action="{{ route('student.update', $student->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="card-body">

            <div class="form-group row">
                <label class="col-2 col-form-label">Roll NO</label>
                <div class="col-10">
                    <input class="form-control" type="hidden" name="id" value="{{$student->id}}" required/>
                    <input class="form-control" type="text" name="roll_no" id="example-text-input" placeholder="Enter roll no" value="{{$student->roll_no}}" required/>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Enrollment NO</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="enrollment_no" id="example-text-input" placeholder="Enter roll no" value="{{$student->enrollment_no}}" required/>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Name</label>
                <div class="col-10">
                    <input class="form-control" type="text" name="name" id="example-text-input" placeholder="Enter name" value="{{$student->name}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Phone No</label>
                <div class="col-10">
                    <input class="form-control" type="tel" name="phone" id="example-text-input" placeholder="Enter phone"  value="{{$student->phone}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">CGPA</label>
                <div class="col-10">
                    <input class="form-control" type="number" name="cgpa" step="0.001" id="example-text-input" placeholder="Enter cgpa"  value="{{$student->cgpa}}" required/>
                </div>
            </div>
            <div class="form-group row">
                <label for="example-search-input" class="col-2 col-form-label">Cloud ID</label>
                <div class="col-10">
                    <input class="form-control" type="email" name="email" id="example-text-input" placeholder="Enter email"  value="{{$student->email}}" required/>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Update</button>
                </div>
                <div class="col-10">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
