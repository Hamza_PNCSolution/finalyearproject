<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth','role:Admin|Teacher|Student|Committee'])->group(function () {

    // For Notes
    Route::resource('/note', 'admin\NoteController')->middleware('role:Admin');

    // For Change Password
    Route::get('/change_password', 'admin\DashboradController@changepassword')->name('change.password');
    Route::post('/updatepassword', 'admin\DashboradController@updatepassword')->name('update.password');

    // For Group scheduler
    Route::get('/scheduler', 'admin\GroupSchedulerController@index')->name('index.scheduler');
    Route::post('/scheduler', 'admin\GroupSchedulerController@generate')->name('store.scheduler')->middleware('role:Admin');

    //For Date of submission
    Route::get('/setdates', 'admin\DateofSubmissionController@index')->name('index.date')->middleware('role:Admin');
    Route::post('/storedate', 'admin\DateofSubmissionController@store')->name('store.date')->middleware('role:Admin');
    Route::get('/editdate/{id}', 'admin\DateofSubmissionController@edit')->name('edit.date')->middleware('role:Admin');
    Route::post('/updatedate', 'admin\DateofSubmissionController@update')->name('update.date')->middleware('role:Admin');

    //For Marks
    Route::get('/adminviewmarks', 'Student\ViewMarksController@adminviewmarks')->name('admin.showmarks')->middleware('role:Admin');

    //For All Activites Show
    Route::get('/adminactivity', 'Teacher\ActivityController@adminindex')->name('all.adminactivity')->middleware('role:Admin');
    Route::get('/detialsactivity/{id}', 'Teacher\ActivityController@adminviewactivity')->name('details.activity')->middleware('role:Admin');

    // For View of Pervious FYP Data
    Route::get('/perviousfyp', 'admin\LastFypDataController@index')->name('all.lastfyp');
    Route::get('/newperviousfyp', 'admin\LastFypDataController@edit_data')->name('new.lastfyp')->middleware('role:Admin');
    Route::post('/storeperviousfyp', 'admin\LastFypDataController@store_data')->name('store.lastfyp')->middleware('role:Admin');
    Route::get('/deleteperviousfyp/{id}', 'admin\LastFypDataController@deleteperviousfyp')->name('delete.lastfyp')->middleware('role:Admin');
    Route::get('keywordserach', 'admin\LastFypDataController@keywordserach' )->name('keyword.search');

    // Admin Side
    Route::get('/dashboard', 'admin\DashboradController@index')->name('dashboard');
    Route::resource('/teacher', 'admin\TeacherController')->middleware('role:Admin');
    Route::resource('/student', 'admin\StudentController')->middleware('role:Admin');
    Route::post('/import', 'admin\ImportController@stdImport')->name('import.student')->middleware('role:Admin');
    Route::post('/thimport', 'admin\ImportController@TeacherImport')->name('import.teacher')->middleware('role:Admin');

    //Slots of All Teacher
    Route::get('/allslots', 'admin\AllSlotsController@index')->name('slots');

    //All Groups
    Route::get('/allgroups', 'admin\GroupsController@index')->name('admin.groups');
    Route::get('/groupdetials/{id}', 'admin\GroupsController@groupdetials')->name('detials.groups');
    Route::post('/assigngroup', 'admin\GroupsController@assigngroup')->name('assign.groups');

    // For Committee Members
    Route::get('/committee', 'admin\CommitteController@index')->name('all.committee')->middleware('role:Admin');
    Route::post('/committee', 'admin\CommitteController@store')->name('store.committee')->middleware('role:Admin');

    // Student Side
    Route::group(['middlerware' => 'role:Student'], function() {

        // For Scheduler
        Route::get('/stdschedule', 'Student\SchedulerController@index')->name('std.scheduler')->middleware('role:Student');


        // For add group student
        Route::get('/group', 'Student\StudentGroupController@viewgroup')->name('view.group')->middleware('role:Student');
        Route::post('/group', 'Student\StudentGroupController@addgroup')->name('add.group')->middleware('role:Student');

        // For Proposal Update
        Route::post('/upproposal', 'Student\StudentGroupController@updateProposal')->name('up.proposal')->middleware('role:Student');

        // For Marks
        Route::get('/viewmarks', 'Student\ViewMarksController@viewmarks')->name('show.marks')->middleware('role:Student');

        // For Activity Store
        Route::get('/stdactivity', 'Teacher\ActivityController@stdindex')->name('std.activity')->middleware('role:Student');
        Route::get('/viewstdactivity/{id}', 'Teacher\ActivityController@stdactivityview')->name('view.activity')->middleware('role:Student');
        Route::post('/storestdactivity', 'Teacher\ActivityController@stdstore')->name('store.stdactivity')->middleware('role:Student');


    });

    // Teacher Side
    Route::group(['middlerware' => 'role:Teacher'], function() {

        // For Panel Meeting
        Route::get('/panelmeeting', 'admin\GroupSchedulerController@panelindex')->name('panel.meeting')->middleware('role:Teacher');
        Route::post('/panelmeeting', 'committe\SchedulerController@panelmeetingstatus')->name('panelmeeting.store');
        Route::get('/groupfeedback/{id}', 'Teacher\MarksController@groupfeedback')->name('group.feeback');


        // For Group
        Route::get('/request/{id}', 'Teacher\TeacherGroupController@viewgrouprequest')->name('view.grouprequest')->middleware('role:Teacher');
        Route::get('/download/{id}', 'Teacher\TeacherGroupController@downloadproposal')->name('download.proposal')->middleware('role:Teacher');
        Route::post('/approved-group/{id}', 'Teacher\TeacherGroupController@groupapproved')->name('group.approved')->middleware('role:Teacher');
        Route::post('/rejected-group/{id}', 'Teacher\TeacherGroupController@groupRejected')->name('group.rejected')->middleware('role:Teacher');
        Route::get('/groups', 'Teacher\TeacherGroupController@groups')->name('all.groups')->middleware('role:Teacher');

        // For Marks
        Route::get('/marks', 'Teacher\MarksController@index')->name('group.marks')->middleware('role:Teacher');
        Route::get('/groupmarks/{id}', 'Teacher\MarksController@groupmarks')->name('add.marks')->middleware('role:Teacher');
        Route::get('/add-marks/{groupid}/{studentid}','Teacher\MarksController@edit')->name('marks.edit')->middleware('role:Teacher');
        Route::post('/add-marks/{groupid}/{studentid}','Teacher\MarksController@edit')->name('marks.edit')->middleware('role:Teacher');

        // For Slots
        Route::get('/slots', 'Teacher\SlotController@index')->name('all.slots')->middleware('role:Teacher|Student');
        Route::post('/addslots', 'Teacher\SlotController@store')->name('store.slots')->middleware('role:Teacher');
        Route::get('/editslots/{id}', 'Teacher\SlotController@edit')->name('edit.slots')->middleware('role:Teacher');
        Route::post('/updateslots/{id}', 'Teacher\SlotController@update')->name('update.slots')->middleware('role:Teacher');
        Route::get('/deleteslots/{id}', 'Teacher\SlotController@delete')->name('delete.slots')->middleware('role:Teacher');


        // For Activities
        Route::get('/activity', 'Teacher\ActivityController@index')->name('all.activity')->middleware('role:Teacher');
        Route::get('/newactivity', 'Teacher\ActivityController@create')->name('new.activity')->middleware('role:Teacher');
        Route::post('/storeactivity', 'Teacher\ActivityController@store')->name('store.activity')->middleware('role:Teacher');
        Route::get('/editactivity/{id}', 'Teacher\ActivityController@editactivity')->name('edit.activity')->middleware('role:Teacher');
        Route::post('/updateactivity', 'Teacher\ActivityController@updateactivity')->name('update.activity')->middleware('role:Teacher');
        Route::get('/viewactivity/{id}', 'Teacher\ActivityController@viewactivity')->name('view.thactivity')->middleware('role:Teacher');

        // For My Porposal
        Route::get('/proposal', 'Teacher\MyPorposalController@index')->name('all.proposal')->middleware('role:Teacher');
        Route::post('/storeproposal', 'Teacher\MyPorposalController@store')->name('store.proposal')->middleware('role:Teacher');
        Route::get('/editproposal/{id}', 'Teacher\MyPorposalController@edit')->name('edit.proposal')->middleware('role:Teacher');
        Route::post('/updateproposal/{id}', 'Teacher\MyPorposalController@update')->name('update.proposal')->middleware('role:Teacher');
        Route::get('/deleteproposal/{id}', 'Teacher\MyPorposalController@delete')->name('delete.proposal')->middleware('role:Teacher');

    });

    Route::group(['middlerware' => 'role:Committee'], function() {
        Route::post('/meetingresponse', 'committe\SchedulerController@meetingstatus')->name('store.response')->middleware('role:Committee');
        Route::get('/schedule', 'committe\SchedulerController@index')->name('show.scheduler');

    });
});
require __DIR__.'/auth.php';
