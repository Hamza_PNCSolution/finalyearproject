<?php

namespace App\Providers;

use App\Models\SchudelPanel;
use App\Models\GroupScheduler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultstringLength(191);
        view()->composer('*', function($view) {
            $scheduler = GroupScheduler::get();
            $checkteacher = SchudelPanel::get();
              $view->with('schedul',$scheduler )->with('checkteacher', $checkteacher);
          });
    }
}
