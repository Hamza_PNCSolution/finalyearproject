<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TeacherImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(($row['name']) && ($row['email'])){
            $user = User::create([
                'name' => trim($row['name']),
                'email' => trim($row['email']),
                'phone' => trim($row['phone']),
                'password' => Hash::make('123'),
                'post' => trim($row['post']),
                'domain' => trim($row['domain']),
            ]);
            $user->assignRole('Teacher');
            return $user;
        }
    }
}
