<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row['name']);
        if( ($row['name']) && ($row['email']) ) {
            $user =  new User([
                'roll_no' => $row['roll_no'],
                'enrollment_no' => $row['enrollment_no'],
                'name' => $row['name'],
                'email' => $row['email'],
                'password' => Hash::make('123'),
                'phone' => $row['phone'],
                'cgpa' => $row['cgpa'],
            ]);
            $user->assignRole('Student');
            return $user;
        }else{
            // dd($row);
        }

    }
}
