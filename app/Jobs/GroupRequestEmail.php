<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GroupRequestEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $teacherData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($teacherData)
    {
        $this->teacherData = $teacherData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $teachUser = $this->teacherData;
        dd($teachUser);
    }
}
