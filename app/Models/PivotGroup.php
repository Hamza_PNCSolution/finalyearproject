<?php

namespace App\Models;

use App\Models\User;
use App\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PivotGroup extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function rqtgroup(){
        return $this->belongsTo(Group::class,'group_id', 'id');
    }

    public function teacher(){
        return $this->belongsTo(User::class,'teacher_id', 'id');
    }
}
