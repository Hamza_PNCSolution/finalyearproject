<?php

namespace App\Models;

use App\Models\Teacher;
use App\Models\PivotGroup;
use App\Models\GroupComment;
use Facade\FlareClient\View;
use App\Models\PanelFeedback;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Group extends Model
{
    use HasFactory, Notifiable;

    protected $guarded = [];

    public function group() {
        return $this->belongsTo(PivotGroup::class,'id', 'group_id');
    }

    public function feedback() {
        return $this->belongsTo(PanelFeedback::class,'id', 'group_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function getPathAttribute($value) {
        return asset('storage/'.$value);
    }

    public function comments(){
        return $this->belongsTo(GroupComment::class,'id', 'group_id');
    }
}
