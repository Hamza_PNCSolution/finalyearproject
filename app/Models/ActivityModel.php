<?php

namespace App\Models;

use App\Models\Group;
use App\Models\ActiviyMeta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ActivityModel extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function activitymeta(){
        return $this->belongsTo(ActiviyMeta::class,'id', 'activity_id');
    }

    public function group(){
        return $this->belongsTo(Group::class,'group_id', 'id');
    }

}
