<?php

namespace App\Models;

use App\Models\SchudelPanel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class GroupScheduler extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function timeslot(){
        return $this->belongsTo(TimeSlot::class,'time_slot_id','id');
    }

    public function group(){
        return $this->belongsTo(Group::class,'group_id','id');
    }

    public function panelteacher(){
        return $this->hasMany(SchudelPanel::class,'schudeler_id','id');
    }

}
