<?php

namespace App\Models;

use App\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MyPorposal extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function group(){
        return $this->belongsTo(Group::class, 'group_id','id');
    }
}
