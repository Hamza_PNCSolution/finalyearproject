<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marks extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function groups(){
        return $this->belongsTo(Group::class,'group_id', 'id');
    }

    public function student(){
        return $this->belongsTo(User::class,'student_id', 'id');
    }

    public function teacher(){
        return $this->belongsTo(User::class,'teacher_id', 'id');
    }
}
