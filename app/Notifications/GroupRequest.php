<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GroupRequest extends Notification implements ShouldQueue
{
    use Queueable;
    private $groupData;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($groupData)
    {
        $this->groupData = $groupData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Notification Alert')
                    ->line('Group Title:  '.$this->groupData['body']['Group_Title'])
                    ->line('Group Description:  '.$this->groupData['body']['Group_Description'])
                    ->line($this->groupData['thankyou'])
                    ->action($this->groupData['data'], $this->groupData['url']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
