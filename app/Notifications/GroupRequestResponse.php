<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GroupRequestResponse extends Notification implements ShouldQueue
{
    use Queueable;
    private $groupResponseData;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($groupResponseData)
    {
        $this->groupResponseData = $groupResponseData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->groupResponseData['body']['reason'] != null){
            return (new MailMessage)
                        ->greeting('Notification Alert')
                        ->line('Comment:  '.$this->groupResponseData['body']['reason'])
                        ->line($this->groupResponseData['body']['by'])
                        ->line($this->groupResponseData['thankyou']);
        }else{
            return (new MailMessage)
                        ->greeting('Notification Alert')
                        ->line($this->groupResponseData['body']['by'])
                        ->line($this->groupResponseData['thankyou']);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
