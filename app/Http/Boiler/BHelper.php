<?php

namespace App\Http\Boiler;

use Illuminate\Http\Request;
// use App\User;
use Session;
use Auth;

class BHelper {

    public static function toast($type,$message,$title='') {
        Session::flash('title',$title);
        Session::flash('type',$type);
        Session::flash('message',$message);
    }

    public static function sweatAlert($type,$message,$title='') {
        Session::flash('title',$title);
        Session::flash('type',$type);
        Session::flash('message',$message);
    }

    static public function checkBrowser() {
        if(isset($_SERVER['HTTP_USER_AGENT'])){
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
        }
        if (stripos( $user_agent, 'Chrome') !== false) { return "chrome"; }
        elseif (stripos( $user_agent, 'Safari') !== false) { return "safari"; }

    }

}
