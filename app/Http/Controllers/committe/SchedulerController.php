<?php

namespace App\Http\Controllers\committe;

use App\Models\User;
use App\Models\Group;
use App\Models\PivotGroup;
use App\Models\PanelFeedback;
use App\Models\GroupScheduler;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SchedulerController extends Controller
{
    public function index()
    {
        if(auth()->user()->hasrole('Committee')){
            $data['teachercount'] = 0;
            $data['page_slug'] = request()->route()->uri();
            $data['teachers'] = User::role('Teacher')->get();
            $data['scheduler'] = GroupScheduler::with('timeslot','group','panelteacher')->get();
            if(count($data['scheduler']) != 0){
                $data['teachercount'] = $data['scheduler'][0]->panelteacher->groupBy('teacher_id')->count();
            }
            return view('fyp.groupscheduler.committee',$data);

        }elseif(auth()->user()->hasrole('Teacher')){
            $GroupID = [];
            $data['teachercount'] = 0;
            $data['page_slug'] = request()->route()->uri();
            $data['teachers'] = User::role('Teacher')->get();
            $groupId = PivotGroup::where(['teacher_id'=> Auth::id()])->groupBy('group_id')->get();
            // dd($groupId);
            if($groupId){
                foreach($groupId as $group){
                    array_push($GroupID, $group->group_id);
                }
                $data['scheduler'] = GroupScheduler::with('timeslot','group','panelteacher')->whereIn('group_id',$GroupID)->get();
                if(count($data['scheduler']) != 0){
                    $data['teachercount'] = $data['scheduler'][0]->panelteacher->groupBy('teacher_id')->count();
                }
            }
            return view('fyp.groupscheduler.teacher',$data);

        }

    }

    public function meetingstatus(Request $request){
        
        // dd($request->toArray());
        $schedule = GroupScheduler::where('id', $request->id)->update(['meeting_response'=> $request->response, 'meeting_status' => 'yes', 'feedback' => $request->feeback]);
        if($schedule){
            $groupStatus = Group::where('id', $request->group_id)->update(['meeting_response'=> $request->response]);
            if($groupStatus){
                return redirect('/schedule')->with(['success' => 'Meeting Status submitted is successfully']);
            }
        }
    }

    public function panelmeetingstatus(Request $request){

        // dd($request->toArray());
        $schedule = GroupScheduler::where('id', $request->id)->update(['meeting_response'=> 'approved', 'meeting_status' => 'yes', 'feedback' => $request->comment]);
        if($schedule){
            if($request->comment){
                $panelfeedback = PanelFeedback::create([
                     'schudeler_id' => $request->id,
                     'group_id' => $request->group_id,
                     'teacher_id' => Auth::id(),
                     'feedback' => $request->comment,
                     'feedback_status' => 'yes',
                ]);
            }
            $groupStatus = Group::where('id', $request->group_id)->update(['meeting_response'=> 'approved']);
            if($groupStatus){
                return redirect()->back()->with(['success' => 'Feeback is submitted successfully']);
            }
        }
    }
}
