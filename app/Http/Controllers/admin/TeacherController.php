<?php

namespace App\Http\Controllers\admin;
use App\Models\User;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['teacher'] = User::role('Teacher')->get();
        return view('fyp.teacher.teacher',  $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['teacher'] = User::role('Teacher')->get();
        return view('fyp.teacher.create-teacher',  $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make('123'),
            'phone' => $request->phone,
            'post' => $request->post,
            'domain' => $request->domain,
        ]);

        $user->assignRole('Teacher');
        if ($user->id) {
            return redirect('/teacher')->with(['success' => 'Teacher is successfully added']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_slug'] = 'teacher';
        $data['teacher'] = User::role('Teacher')->find($id);
        return view('fyp.teacher.update-teacher',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = user::updateorCreate(
            ['id' => $request->id],
            [
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'post' => $request->post,
                'domain' => $request->domain

            ]

        );
        if ($user->id) {
            return redirect('/teacher')->with(['success' => 'Teacher is successfully Updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
