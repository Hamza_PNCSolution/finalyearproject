<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommitteController extends Controller
{
    public function index()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['teachers'] = User::role('Teacher')->get();
        $data['committee'] = User::role('Committee')->get();
        return view('fyp.committee.index',  $data);
    }

    public function store(Request $request){

        // dd($request->toArray());
        $comtMembers = $request->members;
        foreach($comtMembers as $key => $comtMember){

            $user = User::find($comtMember);
            $user->assignRole('Committee');
        }
        return back()->with(['success' => 'committee members are included successfully']);
    }
}
