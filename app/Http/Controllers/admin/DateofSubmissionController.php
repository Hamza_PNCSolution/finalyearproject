<?php

namespace App\Http\Controllers\admin;

use DateTime;
use DateTimeZone;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\DataofSubmission;
use App\Http\Controllers\Controller;
use App\Notifications\LastDateofResponse;
use App\Notifications\LastDateofSubmission;
use Illuminate\Support\Facades\Notification;

class DateofSubmissionController extends Controller
{
    public function index(){

        $data['page_slug'] = request()->route()->uri();
        $data['dateofsubmission'] = DataofSubmission::get();
        $data['dateoftype'] = DataofSubmission::first();
        return view('fyp.dateofsubmission.index',  $data);
    }

    public function store(Request $request){

        // dd($request->toArray());
        $date = DataofSubmission::create([
            'date' => $request->date,
            'type' => $request->type,
        ]);
        if($date){
            if($request->type == 1){
                $lastdate = [
                    'body' => [
                        'note' => 'Last Date of submission is '.$request->date.'',
                    ],
                    'thankyou' => 'Please Submited Your Proposal Before Deadline',
                ];
                $student = User::role('Student')->get();
                Notification::send($student, new LastDateofSubmission($lastdate));
            }else{
                $lastdate = [
                    'body' => [
                        'note' => 'Last Date of submission response is '.$request->date.'',
                    ],
                    'thankyou' => 'Please Accept proposal before deadline',
                ];
                $teacher = User::role('teacher')->get();
                Notification::send($teacher, new LastDateofResponse($lastdate));
            }
            return back()->with('success','Date is assigned successfully');
        }

    }

    public function edit($id){
        $Data = DataofSubmission::where('id', $id)->first();
        return response()->json($Data);
    }

    public function update(Request $request){

        $date = DataofSubmission::where('id', $request->id)->update([
            'date' => $request->date,
        ]);
        if($date){
            return back()->with('success','Date is update successfully');
        }

    }
}
