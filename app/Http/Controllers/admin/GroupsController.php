<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use App\Models\Group;
use App\Models\PivotGroup;
use Illuminate\Http\Request;
use App\Notifications\GroupRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\GroupRequestResponse;
use Illuminate\Support\Facades\Notification;

class GroupsController extends Controller
{
    public function index(){

        $data['page_slug'] = request()->route()->uri();
        $data['groupdata'] = Group::with(['group.teacher','comments.teacher'])->get();
        return view('fyp.groups.allgroups',$data);
    }

    public function groupdetials($id){

        $data['page_slug'] = 'allgroups';
        $group_id = $id;
        $data['students'] = User::whereHas('stdgroup', function ($query) use($group_id) {
            return $query->where('group_id', '=', $group_id);
        })->get();
        $data['groupdata'] = Group::with(['group.teacher','comments.teacher'])->find($id);
        $data['teachers'] = User::role('Teacher')->get();
        if(auth()->user()->hasrole('Admin')){
            return view('fyp.groups.groups_detials',$data);
        }else{
            return response()->json($data);
        }
    }

    public function assigngroup(Request $request){

        // dd($request->toArray());
        $id = $request->group_id;
        $teacherName = User::find($request->teacher_id);
        $groupdata = Group::with('comments.teacher')->find($id);
        $data['students'] = User::whereHas('stdgroup', function ($query) use($id) {
            return $query->where('group_id', '=', $id);
        })->get();
        $pivotgroup = PivotGroup::where('group_id',$id)->update(['is_approved' => 1 , 'is_rejected' => 0]);
        if($pivotgroup){
            $pivotgroup = PivotGroup::where('group_id',$id)->update(['teacher_id' => $request->teacher_id]);
            $group = Group::where('id',$id)->update(['is_approved' => 1 , 'is_rejected' => 0]);
            $groupResponseData = [
                'body' => [
                    'reason' => '',
                    'by' => 'Please contact with your fyp teacher'
                ],
                'thankyou' => 'Admin assigned '.$teacherName->name.' to your group',
            ];
            Notification::send($data['students'], new GroupRequestResponse($groupResponseData));
            $groupData = [
                'body' => [
                    'Group_Title' => $groupdata->title,
                    'Group_Description' => $groupdata->description,
                ],
                'data' => 'Group Details',
                'url' => url('/request/'.$id),
                'thankyou' => 'Admin assigned this group',
            ];
            $teacherData = User::find($request->teacher_id);
            Notification::send($teacherData, new GroupRequest($groupData));
            return redirect('/allgroups')->with('update','Group has been assgined successfully');
        }

    }
}
