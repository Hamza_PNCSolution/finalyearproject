<?php

namespace App\Http\Controllers\admin;

use App\Models\LastFypData;
use Illuminate\Http\Request;
use Spatie\PdfToText\Pdf;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LastFypDataController extends Controller
{
    public function index(){
        $data['page_slug'] = request()->route()->uri();
        $data['data'] = LastFypData::all();
        // $fileName = $data['data'][0]->document;
        // // $path = public_path('storage/'. $fileName);
        // // $reader = new \Asika\Pdf2text;
        // // $output = $reader->decode($path);
        // $pdf_string = Pdf::getText(public_path('storage/') . $fileName);
        // $output = str_replace(array("\n", "\r"), '', trim($pdf_string));
        // dd($output);
        return view('fyp.lastdata.fyp_data' , $data);

    }

    public function edit_data(){

        $data['page_slug'] = request()->route()->uri();
        return view('fyp.lastdata.new_data' , $data);

    }

    public function store_data(Request $request){

        // dd($request->toArray());
        $request->validate([
            'document'=>["required","array","min:1","max:10"],
        ]);
        $files = [];
        if($request->hasFile('document'))
        {
            foreach($request->file('document') as $key => $file){

                $originalname = $file->getClientOriginalName();
                $path = $file->storeAs('fpydatadocument', $originalname, 'public');
                $files[] = $path;
                $file = new LastFypData();
                $file->user_id = Auth::id();
                $file->filename = $originalname;
                $file->document = $files[$key];
                // $path ="C:\Program Files (x86)\Git\mingw64\bin\pdftotext"; // Uswah Laptop Path
                // $path ="C:\Program Files\Git\mingw64\bin\pdftotext"; // Armeen Laptop Path
                $pdf_string = Pdf::getText('storage/'.$file->document, $path);
                $output = str_replace(array("\n", "\r"), '', trim($pdf_string));
                $file->pdf_text = $output;
                $file->save();
            }
        }
        return redirect('/perviousfyp')->with(['success' => 'File is upload Successfully']);
    }

    public function deleteperviousfyp($id){

        $data = LastFypData::where('id', $id)->delete();
        if($data){
            return redirect('/perviousfyp')->with(['delete' => 'Data is delete Successfully']);
        }

    }

    public function keywordserach(Request $request){

        $data['page_slug'] = request()->route()->uri();
        $data['data'] = LastFypData::where('pdf_text', 'like', '%' . $request->get('keyword') . '%')->get();
        return view('fyp.lastdata.fyp_data' , $data);
    }
}
