<?php


namespace App\Http\Controllers\admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Group;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\PivotGroup;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use App\Models\DataofSubmission;
use App\Http\Controllers\Controller;
use App\Models\ActivityModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DashboradController extends Controller
{
    public function index(){
        $data['page_slug'] = request()->route()->uri();
        $user = User::find(Auth::id());

        if($user->hasRole('Admin')){
            $data['student'] = User::role('Student')->count();
            $data['teacher'] = User::role('Teacher')->count();
            $data['committee'] = User::role('Committee')->count();
            $data['groups'] = Group::count();
            $data['grouplist'] = Group::latest()->take(5)->get();
            $data['activities'] = ActivityModel::latest()->take(5)->get();

            return view('fyp.dashboard', $data);

        }elseif($user->hasRole('Student')){

            $data['project'] = Group::whereHas('group', function ($query) {
                return $query->where(['student_id' => Auth::id()]);
                })->get();
            $data['datetime'] = DataofSubmission::where('type','1')->first();
            if($data['datetime']){
                $data['converteddate'] = Carbon::parse($data['datetime']->date)->format('Y-m-d h:i A');
            }
            if(count($data['project']) > 0 ){
                $groupId = PivotGroup::where(['student_id'=> Auth::id()])->orWhere(['teacher_id'=> Auth::id() ])->first();
                $tech_id = $groupId->teacher_id;
                $groupId = $groupId->group_id;
                $data['students'] = User::whereHas('stdgroup', function ($query) use($groupId) {
                    return $query->where(['group_id'=>$groupId]);
                })->get();
                $data['teacher'] = User::whereHas('teachergroup', function ($query) use($groupId) {
                    return $query->where(['group_id'=>$groupId, 'is_approved' => 1]);
                })->first();
                $data['activities'] = ActivityModel::where('group_id',$groupId)->count();
                $data['assignedactivity'] = ActivityModel::where('group_id',$groupId)->latest()->take(5)->get();
                return view('fyp.Studentside.StudentDashboard', $data);
            }else{
                return view('fyp.Studentside.StudentDashboard', $data);
            }



        }elseif($user->hasRole('Teacher')){

            $data['datetime'] = DataofSubmission::first();
            if($data['datetime']){
                $data['converteddate'] = Carbon::parse($data['datetime']->date)->format('Y-m-d h:i A');
            }
            $data['project'] = Group::whereHas('group', function ($query) {
                return $query->where(['teacher_id' => Auth::id()]);
                })->get();

            if(count($data['project']) > 0 ){
                $groupId = PivotGroup::where(['teacher_id'=> Auth::id()])->first();
                $groupId = $groupId->group_id;
                $data['students'] = User::whereHas('stdgroup', function ($query) use($groupId) {
                    return $query->where(['group_id'=>$groupId]);
                })->get();
                $data['groupRequest'] = PivotGroup::where(['teacher_id'=> Auth::id() , 'is_approved' => 0 , 'is_rejected' => 0])->groupBy('group_id')->get()->count();
                $data['groupApproved'] = PivotGroup::where(['teacher_id'=> Auth::id() , 'is_approved' => 1 ])->groupBy('group_id')->get()->count();
                $data['groupStudent'] = PivotGroup::where(['teacher_id'=> Auth::id() , 'is_approved' => 1 ])->get()->count();
                $data['activities'] = ActivityModel::where(['teacher_id'=> Auth::id()])->latest()->take(5)->get();
                return view('fyp.TeacherSide.TeacherDashboard', $data);
            }else{

                return view('fyp.TeacherSide.TeacherDashboard', $data);
            }
        }
    }

    public function changepassword(){

        $data['page_slug'] = request()->route()->uri();
        return view('fyp.changepassword', $data);
    }

    public function updatepassword(Request $request){

        $request->validate([
            'old_password' => ['required', new MatchOldPassword],
            // 'new_password' => 'required|min:8|regex:/^(?=\S*[a-z])(?=\S*[!@#$&*])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
            'new_password' => 'required|min:8',
            'conform_password' => 'required|same:new_password',
        ]);

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        return back()->with('success', 'Password change successfully');
    }
}
