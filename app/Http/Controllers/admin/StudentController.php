<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_slug'] = request()->route()->uri();
        $data['student'] = User::role('Student')->get();
        return view('fyp.student.student',  $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_slug'] = request()->route()->uri();
        return view('fyp.student.create-student',  $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make('123'),
            'roll_no' => $request->roll_no,
            'enrollment_no' => $request->enrollment_no,
            'phone' => $request->phone,
            'cgpa' => $request->cgpa,
        ]);

        $user->assignRole('Student');
        if ($user->id) {
            return redirect('/student')->with(['success' => 'Student is successfully added']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_slug'] = 'student';
        $data['student'] = User::role('Student')->find($id);
        return view('fyp.student.update-student',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = user::updateorCreate(
            ['id' => $request->id],
            [
                'name' => $request->name,
                'email' => $request->email,
                'roll_no' => $request->roll_no,
                'enrollment_no' => $request->enrollment_no,
                'phone' => $request->phone,
                'cgpa' => $request->cgpa,
            ]
        );
        if ($user->id) {
            return redirect('/student')->with(['success' => 'Student is successfully updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
