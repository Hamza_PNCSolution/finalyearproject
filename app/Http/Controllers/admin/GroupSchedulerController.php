<?php

namespace App\Http\Controllers\admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Group;
use App\Models\TimeSlot;
use App\Models\PivotGroup;
use App\Models\SchudelPanel;
use Illuminate\Http\Request;
use App\Models\GroupScheduler;
use App\Http\Controllers\Controller;
use App\Models\PanelFeedback;
use Illuminate\Support\Facades\Auth;

class GroupSchedulerController extends Controller
{
    public function index(){

        $data['teachercount'] = 0;
        $data['page_slug'] = request()->route()->uri();
        $data['teachers'] = User::role('Teacher')->get();
        $data['scheduler'] = GroupScheduler::with('timeslot','group','panelteacher')->orderBy('room','ASC')->get();
        if(count($data['scheduler']) != 0){
            $data['teachercount'] = $data['scheduler'][0]->panelteacher->groupBy('teacher_id')->count();
        }
        // dd($data['scheduler'][0]->group->group->teacher->name);
        return view('fyp.groupscheduler.index',$data);

    }

    public function generate(Request $request){

        // dd($request->toArray());
        // For Day Checking
        $day = Carbon::parse($request->startdate)->format('l');
        if($day == "Thursday"){
            $Date = Carbon::create($request->startdate)->addDay(3)->format("m/d/Y");
        }else{
            $Date = Carbon::create($request->startdate)->addDay(1)->format("m/d/Y");
        }

        // For Pervious Data Delete
        $data['scheduler'] = GroupScheduler::count();
        if($data['scheduler'] != 0){
            $deleteschudeler = GroupScheduler::where('meeting_status','yes')->delete();
            $deletepanel = SchudelPanel::where('meeting_assigned','yes')->delete();
            $deletepanelfeedback = PanelFeedback::where('feedback_status','yes')->delete();
        }

        // For Defense Two
        if($request->for == 'defense_two'){
            $groups = Group::where(['is_approved' => '1', 'meeting_response' => 'rejected'])->get();
        }else{
            $groups = Group::where('is_approved','1')->get();
        }

        // For new time table
        $timeslot = array();
        $timeslots = TimeSlot::get();
        for($i = 0; $i < 36; $i++ ){
            array_push($timeslot,$timeslots[$i]->id);
        }
            $i = 0;
            $j = 0;
            $k = 0;
            foreach($groups as $index => $group) {
                if($index < 18) {
                    if($index < 9){
                        $group_scheduler = GroupScheduler::create([
                            'group_id' => $group->id,
                            'time_slot_id' => $timeslot[$i],
                            'room' => $request->room[0],
                            'meeting_date' => $request->startdate,
                            'meeting_type' => $request->for,
                            'meeting_status' => 'no',
                        ]);
                        $i++;
                    }else{
                        $group_scheduler = GroupScheduler::create([
                            'group_id' => $group->id,
                            'time_slot_id' => $timeslot[$k],
                            'room' => $request->room[1],
                            'meeting_date' => $request->startdate,
                            'meeting_type' => $request->for,
                            'meeting_status' => 'no',
                        ]);
                        $k++;
                    }
                }elseif($index > 18){
                    $group_scheduler = GroupScheduler::create([
                        'group_id' => $group->id,
                        'time_slot_id' => $timeslot[$j],
                        'room' => $request->room[0],
                        'meeting_date' => $Date,
                        'meeting_type' => $request->for,
                        'meeting_status' => 'no',
                    ]);
                    $j++;
                }
                if($group_scheduler){
                    // For Panel Teacher
                    if($request->teachers){
                        foreach($request->teachers as $teacherID){
                            $panelteacher = SchudelPanel::create([
                                'schudeler_id' => $group_scheduler->id,
                                'teacher_id' => $teacherID,
                                'meeting_assigned' => 'yes',
                            ]);
                        }
                    }
                    $group_status = Group::where('id',$group->id)->update(['time_assigned' => 'yes']);
                }
                // dd($remainingroom);
            }
            return redirect('/scheduler')->with(['success' => 'Group Scheduler is created successfully']);
    }

    // For Panel Meeting

    public function panelindex(){
        $data['teachercount'] = 0;
        $data['page_slug'] = request()->route()->uri();
        $data['scheduler'] = GroupScheduler::with('timeslot','group','panelteacher')->get();
        if(count($data['scheduler']) != 0){
            $data['teachercount'] = $data['scheduler'][0]->panelteacher->groupBy('teacher_id')->count();
        }
        return view('fyp.TeacherSide.Meeting.index',$data);
    }

}

