<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Slot;
use Illuminate\Http\Request;

class AllSlotsController extends Controller
{
    public function index(){
        $data['page_slug'] = request()->route()->uri();
        $data['slots'] = Slot::with('user')->get();
        // dd($data['slots']->user->name);
        return view('fyp.teacherslot.allslots',$data);
    }
}
