<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Imports\StudentImport;
use App\Http\Controllers\Controller;
use App\Imports\TeacherImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function stdImport(Request $request){

        // dd($request->all());
        $import = Excel::import(new StudentImport, $request->file);
        // dd($import);
        if ($import) {
            return redirect('/student')->with(['success' => 'Student is import successfully added']);
        }
    }

    public function TeacherImport(Request $request){

        // dd($request->toArray());
        $import = Excel::import(new TeacherImport, $request->file);
        if ($import) {
            return redirect('/teacher')->with(['success' => 'Teacher is import successfully added']);
        }
    }
}
