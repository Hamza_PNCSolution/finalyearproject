<?php

namespace App\Http\Controllers\Teacher;

use App\Models\User;
use App\Models\PivotGroup;
use App\Models\GroupScheduler;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SchedulerController extends Controller
{
    public function index(){

        $GroupID = [];
        $data['teachercount'] = 0;
        $data['page_slug'] = request()->route()->uri();
        $data['teachers'] = User::role('Teacher')->get();
        $groupId = PivotGroup::where(['teacher_id'=> Auth::id()])->groupBy('group_id')->get();
        // dd($groupId);
        if($groupId){
            foreach($groupId as $group){
                array_push($GroupID, $group->group_id);
            }
            $data['scheduler'] = GroupScheduler::with('timeslot','group','panelteacher')->whereIn('group_id',$GroupID)->get();
            if(count($data['scheduler']) != 0){
                $data['teachercount'] = $data['scheduler'][0]->panelteacher->groupBy('teacher_id')->count();
            }
        }
        return view('fyp.groupscheduler.teacher',$data);
    }
}
