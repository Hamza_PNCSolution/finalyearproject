<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Slot;
use App\Models\PivotGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SlotController extends Controller
{
    public function index(){
        $data['page_slug'] = request()->route()->uri();
        if(auth()->user()->hasrole('Student')){
            $teacherId = PivotGroup::where(['student_id'=> Auth::id()])->first();
            $data['slots'] = Slot::where('teacher_id', $teacherId->teacher_id)->get();
        }else{
            $data['slots'] = Slot::where('teacher_id', Auth::id())->get();
        }
        return view('fyp.TeacherSide.Slots.slots' , $data);
    }

    public function store(Request $request){
        parse_str($request->form , $form);
        $slot = Slot::create([
            'teacher_id' => Auth::id(),
            'date' => $form['date'],
            'start_time' => $form['starttime'],
            'end_time' => $form['endtime'],
            'room' => $form['room'],
        ]);
        return $slot;
    }

    public function edit($id){

        $slot = Slot::where(['teacher_id' => Auth::id(), 'id' => $id])->get();
        // dd($slot);
        return response()->json($slot);
    }

    public function update(Request $request)
    {
        // dd($request->toArray());
        $data = Slot::where('id',$request->id)->update([
            'date' => $request->date,
            'start_time' => $request->starttime,
            'end_time' => $request->endtime,
            'room' => $request->room,
        ]);
        return $data;
    }

    public function delete($id){
        $Data = Slot::find($id)->delete();
        return back()->with('delete','Slot has been delete successfully');
    }
}
