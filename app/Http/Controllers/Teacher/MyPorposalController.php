<?php

namespace App\Http\Controllers\Teacher;

use App\Models\MyPorposal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MyPorposalController extends Controller
{
    public function index(){

        $data['page_slug'] = request()->route()->uri();
        $data['proposals'] = MyPorposal::where(['teacher_id' => Auth::id(), 'is_delete' => '0'])->get();
        return view('fyp.TeacherSide.Proposal.index' , $data);

    }

    public function store(Request $request){
        parse_str($request->form , $form);
        $proposal = MyPorposal::create([
            'teacher_id' => Auth::id(),
            'title' => $form['title'],
            'description' => $form['description'],
        ]);
        return $proposal;
    }

    public function edit($id){

        $proposal = MyPorposal::where(['teacher_id' => Auth::id(), 'id' => $id])->first();
        return response()->json($proposal);
    }

    public function update(Request $request)
    {
        $data = MyPorposal::where('id',$request->id)->update([
            'title' => $request->title,
            'description' => $request->description,
        ]);
        return $data;
    }

    public function delete($id){
        $data = MyPorposal::where('id',$id)->update([
            'is_delete' => '1'
        ]);
        return back()->with('delete','Proposal has been delete successfully');
    }
}
