<?php

namespace App\Http\Controllers\Teacher;

use App\Models\User;
use App\Models\Group;
use App\Models\ActiviyMeta;
use App\Models\StudentMeta;
use Illuminate\Http\Request;
use App\Models\ActivityModel;
use App\Http\Controllers\Controller;
use App\Notifications\GroupActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ActivityController extends Controller
{
    public function index(){

        $data['page_slug'] = request()->route()->uri();
        $data['activity'] = ActivityModel::with('group')->where('teacher_id', Auth::id())->get();
        return view('fyp.TeacherSide.Activity.activities' , $data);

    }

    public function adminindex(){

        $data['page_slug'] = request()->route()->uri();
        $data['activity'] = ActivityModel::with('group')->get();
        return view('fyp.TeacherSide.Activity.activities' , $data);

    }

    public function adminviewactivity($id){

        $data['page_slug'] = request()->route()->uri();
        $data['activity'] = ActivityModel::with('activitymeta')->where('id',$id)->first();
        $data['groupleader'] = StudentMeta::where('student_id', Auth::id())->first();
        return view('fyp.TeacherSide.Activity.view-activity' , $data);

    }

    public function viewactivity($id){

        $data['page_slug'] = request()->route()->uri();
        $data['activity'] = ActivityModel::with('activitymeta')->where('id',$id)->first();
        $data['groupleader'] = StudentMeta::where('student_id', Auth::id())->first();
        return view('fyp.TeacherSide.Activity.view-activity' , $data);

    }

    // For Student View
    public function stdindex(){

        $data['page_slug'] = request()->route()->uri();
        $data['groups'] = Group::whereHas('group', function ($query) {
            return $query->where(['student_id' => Auth::id()]);
        })->get();
        $data['activity'] = ActivityModel::where('group_id',$data['groups'][0]->id)->get();
        $data['groupleader'] = StudentMeta::where('student_id', Auth::id())->first();
        return view('fyp.Studentside.Activity.activity' , $data);

    }

    // For Student Activity Submission Form
    public function stdactivityview($id){

        $data['page_slug'] = request()->route()->uri();
        $data['activity'] = ActivityModel::with('activitymeta')->where('id',$id)->first();
        $data['groupleader'] = StudentMeta::where('student_id', Auth::id())->first();
        return view('fyp.Studentside.Activity.view-activity' , $data);

    }

    public function create(){

        $data['page_slug'] = request()->route()->uri();
        $data['groups'] = Group::whereHas('group', function ($query) {
            return $query->where(['teacher_id' => Auth::id(), 'is_approved' =>1]);
        })->get();
        return view('fyp.TeacherSide.Activity.new_activites' , $data);

    }

    public function store(Request $request){

        // dd($request->toArray());
        $id = $request->group;
        $data['students'] = User::whereHas('stdgroup', function ($query) use($id) {
            return $query->where('group_id', '=', $id);
        })->get();

        $activity = ActivityModel::create([
            'group_id' => $request->group,
            'teacher_id' => Auth::id(),
            'title' => $request->title,
            'description' => $request->description,
            'document' => $request->document,
            'end_date' => $request->end_date,
            'status' => 'Assigned',
        ]);

        if($activity){
            $groupData = [
                'body' => [
                    'title' => $request->title,
                    'description' => $request->description,
                ],
                'data' => 'Activity Details',
                'url' => url('/stdactivity'),
                'thankyou' => 'Please Check your activity before deadline',
            ];
            Notification::send($data['students'], new GroupActivity($groupData));
            return redirect('/activity')->with(['success' => 'Activity is assigned Successfully']);
        }

    }

    // For Student Submit Activity Form
    public function stdstore(Request $request){

        // dd($request->toArray());
        if($request->hasFile('document')){
            $imagePath = $request->file('document');
            $imageName  = uniqid('', true) . '.' . $imagePath->getClientOriginalExtension();
            $path = $request->file('document')->storeAs('activitydocument', $imageName, 'public');
        }
        $activity = ActiviyMeta::create([
            'activity_id' => $request->activity_id,
            'student_id' => Auth::id(),
            'comment' => $request->comment,
            'document' => $path ?? '',
            'submitted_date' => $request->submitted_date,
        ]);
        if($activity){
            $activitystatus = ActivityModel::where('id', $request->activity_id)->update(['status' => 'Submit']);
            if($activitystatus){
                return redirect('/stdactivity')->with(['success' => 'Activity is Submit Successfully']);
            }
        }

    }

    public function editactivity($id){

        $data['page_slug'] = request()->route()->uri();
        $data['activity'] = ActivityModel::with('group')->where('id', $id)->first();
        $data['groups'] = Group::whereHas('group', function ($query) {
            return $query->where(['teacher_id' => Auth::id(), 'is_approved' =>1]);
        })->get();
        return view('fyp.TeacherSide.Activity.edit_activities' , $data);

    }

    public function updateactivity(Request $request){

        $activity = ActivityModel::where('id', $request->activity_id)->update([
                'title' => $request->title,
                'group_id' => $request->group,
                'end_date' => $request->end_date ,
                'description' => $request->description,
                'document' => $request->document]);
        if($activity){
            return redirect('/activity')->with(['success' => 'Activity is update Successfully']);
        }

    }

}
