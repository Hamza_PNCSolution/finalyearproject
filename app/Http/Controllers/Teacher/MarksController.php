<?php

namespace App\Http\Controllers\Teacher;

use App\Models\User;
use App\Models\Group;
use App\Models\PivotGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Marks;
use App\Http\Common\Helper;
use App\Models\GroupScheduler;
use App\Models\PanelFeedback;
use App\Models\SchudelPanel;

class MarksController extends Controller
{
    public function index(){

        $data['page_slug'] = request()->route()->uri();

        $data['groups'] = Group::with('feedback')->whereHas('group', function ($query) {
            return $query->where(['teacher_id' => Auth::id(), 'is_approved' => 1]);
        })->get();
        // $checkteacher = SchudelPanel::where('teacher_id', Auth::id())->get();
        // dd($checkteacher);
        return view('fyp.TeacherSide.Marks.groupmarks' , $data);

    }

    public function groupmarks(Request $request, $id){

        $data['page_slug'] = request()->route()->uri();
        $group_id = $id;
        $tt_id = Auth::id();
        $data['students'] = User::whereHas('stdgroup', function ($query) use($group_id) {
            return $query->where('group_id', '=', $group_id);
        })->get();
        $data['groupdata'] = Group::find($id);
        // dd($data['students']);
        return view('fyp.TeacherSide.Marks.edit-groupmarks' , $data);

    }

    public function edit(Request $request, $groupid,$studentid){

        if($request->isMethod('post')){

        $group_id = $groupid;
        $student_id = $studentid;
        $teacher_id = Auth::id();
        $request->validate([
            'midevolution'=>'required|numeric|min:0|max:40',
            'midyearevolution'=>'required|numeric|min:0|max:60',
            'finalyearevolution'=>'required|numeric|min:0|max:100',
            ]);

            $data =[
                'group_id'=> $group_id,
                'student_id'=> $student_id,
                'teacher_id'=> $teacher_id,
                'mid_semester_evaluation'=>$request->midevolution,
                'mid_year_evaluation'=>$request->midyearevolution,
                'final_year_evaluation'=>$request->finalyearevolution,
            ];

            if($request->islocked_mid_marks){
                $data['islocked_mid_marks'] = 1;

            }

            if($request->islocked_midyear_marks){
                $data['islocked_midyear_marks'] = 1;

            }

            if($request->islocked_finalyear_marks){
                $data['islocked_finalyear_marks'] = 1;

            }


            $occur = marks::where('student_id',$student_id)->first();

            if($occur != null){

                marks::where('student_id',$student_id)->update($data);
                Helper::toast('success','Successfully Updated');

            }else {

            marks::create($data);
            Helper::toast('success','Successfully Added');

            }
            // return $data;
            return redirect(route('group.marks'));

        }
        else {

            $data['page_slug'] = request()->route()->uri();
            $group_id = $groupid;
            $student_id = $studentid;
            $tt_id = Auth::id();
            $data['students'] = User::whereHas('stdgroup', function ($query) use($group_id,$student_id) {
                return $query->where(['group_id'=> $group_id,'student_id'=> $student_id]);
            })->get();
            $data['groupdata'] = Group::find($group_id);
            $data['stdmarks'] = marks::where('student_id',$student_id)->first();

            // dd($data['stdmarks']);
            return view('fyp.TeacherSide.Marks.add_marks' , $data);

        }

    }

    public function groupfeedback($id){

        $feedback = PanelFeedback::with('teacher')->where('group_id',$id)->get();
        return response()->json($feedback);
    }
}
