<?php

namespace App\Http\Controllers\Teacher;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Group;
use App\Models\MyPorposal;
use App\Models\PivotGroup;
use App\Models\GroupComment;
use Illuminate\Http\Request;
use App\Models\DataofSubmission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\GroupRequestResponse;
use Illuminate\Support\Facades\Notification;

class TeacherGroupController extends Controller
{
    public function viewgrouprequest($id){
        $data['page_slug'] = request()->route()->uri();
        $data['datetime'] = DataofSubmission::where('type','2')->first();
        if($data['datetime']){
            $data['converteddate'] = Carbon::parse($data['datetime']->date)->format('Y-m-d h:i A');
        }
        $data['groupdata'] = Group::find($id);
        $data['checkgroup'] = PivotGroup::where('group_id',$id)->where('teacher_id', Auth::id())->first();
        $data['proposals'] = MyPorposal::where(['teacher_id' => Auth::id(), 'assigned' => 'no'])->get();
        if($data['checkgroup']){
            $group_id = $id;
            $tt_id = Auth::id();
            $data['students'] = User::whereHas('stdgroup', function ($query) use($group_id) {
                return $query->where('group_id', '=', $group_id);
            })->get();
            // dd($data['students']);
            return view('fyp.TeacherSide.groups_details',$data);
        }else{
            abort(404);
        }
    }

    public function downloadproposal($id){

        $download = Group::where('id',$id)->first()->path;
        return $download;
    }

    public function groupapproved(Request $request,$id){
        // dd($request->toArray());
        $teacherName = User::find(Auth::id());
        $data['students'] = User::whereHas('stdgroup', function ($query) use($id) {
            return $query->where('group_id', '=', $id);
        })->get();
        if($request->comment){
            $comment = GroupComment::create([
                'group_id' => $id,
                'teacher_id' => Auth::id(),
                'proposal_id' => $request->proposal,
                'comment' => $request->comment,
                'for' => $request->for,
            ]);
        }
        if($request->proposal){
            $proposal = MyPorposal::where('id',$request->proposal)->update(['group_id' => $id, 'assigned' => 'yes']);
            $groupteacherproposal = Group::where('id', $id)->update(['teacher_proposal' => $request->proposal]);
        }
        $pivotgroup = PivotGroup::where('group_id',$id)->where('teacher_id', Auth::id())->update(['is_approved' => 1]);
        if($pivotgroup){
            $pivotgroup = PivotGroup::where('group_id',$id)->whereNotIn('teacher_id', [Auth::id()])->delete();
            $group = Group::where('id',$id)->update(['is_approved' => 1 , 'is_rejected' => 0]);
            $groupResponseData = [
                'body' => [
                    'reason' => $request->comment,
                    'by' => $teacherName->name,
                ],
                'thankyou' => 'Your proposal is accepted',
            ];
            Notification::send($data['students'], new GroupRequestResponse($groupResponseData));
        }
        return redirect()->route('dashboard');

    }

    public function groupRejected(Request $request,$id){
        $teacherName = User::find(Auth::id());
        $data['students'] = User::whereHas('stdgroup', function ($query) use($id) {
            return $query->where('group_id', '=', $id);
        })->get();
        if($request->comment){
            $comment = GroupComment::create([
                'group_id' => $id,
                'teacher_id' => Auth::id(),
                'comment' => $request->comment,
                'for' => $request->for,
            ]);
        }
        $pivotgroup = PivotGroup::where('group_id',$id)->where('teacher_id', Auth::id())->update(['is_rejected' => 1, 'teacher_id' => 0]);
        if($pivotgroup){
            $group = Group::where('id',$id)->update(['is_rejected' => 1]);
            $groupResponseData = [
                'body' => [
                    'reason' => $request->comment,
                    'by' => $teacherName->name,
                ],
                'thankyou' => 'Please resubmit your proposal',
            ];
            Notification::send($data['students'], new GroupRequestResponse($groupResponseData));
        }
        return redirect()->route('dashboard');

    }

    public function groups(){
        $data['page_slug'] = request()->route()->uri();
        $data['groups'] = Group::whereHas('group', function ($query) {
            return $query->where(['teacher_id' => Auth::id(), 'is_approved' =>1]);
        })->get();
        return view('fyp.TeacherSide.Groups.groups', $data);
    }
}
