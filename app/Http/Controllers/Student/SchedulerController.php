<?php

namespace App\Http\Controllers\Student;

use App\Models\User;
use App\Models\PivotGroup;
use App\Models\GroupScheduler;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SchedulerController extends Controller
{
    public function index(){

        $data['teachercount'] = 0;
        $data['page_slug'] = request()->route()->uri();
        $data['teachers'] = User::role('Teacher')->get();
        $groupId = PivotGroup::where(['student_id'=> Auth::id()])->first();
        if($groupId){
            $data['scheduler'] = GroupScheduler::with('timeslot','group','panelteacher.teacher')->where('group_id',$groupId->group_id)->get();
            if(count($data['scheduler']) != 0){
                $data['teachercount'] = $data['scheduler'][0]->panelteacher->groupBy('teacher_id')->count();
            }
        }
        // dd($data['scheduler'][0]->panelteacher[0]->teacher);
        return view('fyp.groupscheduler.index',$data);
    }
}
