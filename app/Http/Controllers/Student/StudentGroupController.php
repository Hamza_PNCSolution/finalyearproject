<?php

namespace App\Http\Controllers\Student;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Group;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\MyPorposal;
use App\Models\PivotGroup;
use App\Models\StudentMeta;
use App\Models\GroupComment;
use Illuminate\Http\Request;
use App\Jobs\GroupRequestEmail;
use App\Models\DataofSubmission;
use App\Notifications\GroupRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PHPUnit\TextUI\XmlConfiguration\Groups;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\admin\StudentController;

class StudentGroupController extends Controller
{
    public function viewgroup(){
        $data['page_slug'] = request()->route()->uri();
        $data['datetime'] = DataofSubmission::where('type','1')->first();
        if($data['datetime']){
            $data['convertedate'] = Carbon::parse($data['datetime']->date)->format('Y-m-d h:i A');
        }
        $data['teachers'] = User::role('Teacher')->get();
        $data['students'] = User::role('Student')->where('is_group', 0 )->get();
        $data['checkgroup'] = User::role('Student')->with(['stdgroup'])->where(['id' => Auth::id() , 'is_group' => 1 ])->first();
        if($data['checkgroup'] !=  null){
            $data['proposal'] = MyPorposal::with('group')->where(['teacher_id' => $data['checkgroup']->stdgroup->teacher_id, 'group_id' => $data['checkgroup']->stdgroup->group_id])->first();
            $data['comment'] = GroupComment::with('teacher')->where('group_id', $data['checkgroup']->stdgroup->group_id )->get();
        }
        return view('fyp.Studentside.groupadd',$data);
    }

    public function addgroup(Request $request){
        $data['page_slug'] = request()->route()->uri();
        $request->validate([
            'title'=>'required',
            'teachers'=>["required","array","min:1","max:3"],
            'students'=>["required","array","min:3","max:4"],
            'description'=>'required',
            'proposal'=>'required|mimes:pdf,doc,docx,xlsx|max:10000',
            ]);

            $data =[
                'title'=>$request->title,
                'description'=>$request->description,
            ];

            if ($request->hasFile('proposal')) {
                $imagePath = $request->file('proposal');
                $imageName  = uniqid('', true) . '.' . $imagePath->getClientOriginalExtension();
                $path = $request->file('proposal')->storeAs('projectporpsal', $imageName, 'public');
                $data['path'] = $path;
            }

        $LastestGroup = Group::create($data)->id;

        if($LastestGroup){
            $groupleader = StudentMeta::create([
                'student_id' => Auth::id(),
                'group_leader' => '1',
            ]);

            // For Email Notificaion of Group Request
            $groupData = [
                'body' => [
                    'Group_Title' => $request->title,
                    'Group_Description' => $request->description,
                ],
                'data' => 'Group Details',
                'url' => url('/request/'.$LastestGroup),
                'thankyou' => 'Please check group request before deadline',
            ];
            $teacherData = User::find($request->teachers);
            Notification::send($teacherData, new GroupRequest($groupData));
        }

        foreach ($request->students as $stid)
        {
            foreach ($request->teachers as $tdid){

                $data =[
                    'group_id'=> $LastestGroup,
                    'student_id'=> $stid,
                    'teacher_id'=> $tdid,
                ];

                PivotGroup::create($data);
            }
            User::where('id',$stid )->update(['is_group' => 1]);
        }

        return redirect()->route('dashboard');
    }

    public function updateProposal(Request $request){
        // dd($request->group_id);
        $file = $request->file('new_proposal');
        $originalname = $file->getClientOriginalName();
        $path = $file->storeAs('projectporpsal', $originalname, 'public');
        $updateProposal = Group::where('id', $request->group_id)->update([
            'title'=>$request->title,
            'description'=>$request->description,
            'path' => $path,
            'teacher_proposal' => $request->proposal_id,
        ]);
        if($updateProposal){
            return back()->with('update','Proposal has been update successfully');
        }
    }
}
