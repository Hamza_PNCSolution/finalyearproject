<?php

namespace App\Http\Controllers\Student;

use App\Models\Marks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ViewMarksController extends Controller
{
    public function viewmarks(){
        // dd(Auth::id());
        $data['page_slug'] = request()->route()->uri();
        $data['marks'] = Marks::where('student_id' , Auth::id())->get();
        return view ('fyp.Studentside.view_marks' , $data);
    }

    public function adminviewmarks(){
        // dd(Auth::id());
        $data['page_slug'] = request()->route()->uri();
        $data['marks'] = Marks::with('groups','student','teacher')->get();
        // dd($data['marks']);
        return view ('fyp.marks.index' , $data);
    }
}
