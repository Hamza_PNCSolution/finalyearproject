<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('group_id')->unsigned()->index();
            $table->foreignId('student_id')->unsigned()->index();
            $table->foreignId('teacher_id')->unsigned()->index();
            $table->string('mid_semester_evaluation')->nullable();
            $table->string('mid_year_evaluation')->nullable();
            $table->string('final_year_evaluation')->nullable();
            $table->boolean('islocked_mid_marks')->default(0);
            $table->boolean('islocked_midyear_marks')->default(0);
            $table->boolean('islocked_finalyear_marks')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks');
    }
}
