<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchudelPanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schudel_panels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('schudeler_id')->nullable();
            $table->unsignedBigInteger('teacher_id')->nullable();
            $table->string('meeting_assigned')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schudel_panels');
    }
}
