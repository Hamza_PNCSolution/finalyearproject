<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePivotGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_groups', function (Blueprint $table) {
            $table->id();
            $table->foreignId('group_id')->unsigned()->index();
            $table->foreignId('student_id')->unsigned()->index();
            $table->foreignId('teacher_id')->unsigned()->index();
            $table->string('is_approved')->default(0);
            $table->string('is_rejected')->default(0);
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_groups');
    }
}
