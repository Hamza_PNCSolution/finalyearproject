<?php

namespace Database\Seeders;

use App\Models\TimeSlot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // // \App\Models\User::factory(10)->create();
        \App\Models\User::factory()->create(['name' => 'Administrator','email' => 'admin@admin.com']);
        $student = Role::create(['name' => 'Student']);
        $admin = Role::create(['name' => 'Admin']);
        $teacher = Role::create(['name' => 'Teacher']);
        $committee = Role::create(['name' => 'Committee']);
        $user = User::find('1');
        $user->assignRole($admin);

        $date_one = Carbon::parse("09:00");
        $date_two = Carbon::parse("09:00");
        $date_three = Carbon::parse("09:00");
        $date_four = Carbon::parse("09:00");
        for($i = 0; $i < 36; $i++) {
            $timeslot = TimeSlot::create([
                'defense' => $date_one->addMinute(15)->format("H:i"),
                'mid_semester' => $date_two->addMinute(20)->format("H:i"),
                'mid_year' => $date_three->addMinute(30)->format("H:i"),
                'final_evaluation' => $date_four->addMinute(30)->format("H:i"),
            ]);
        }
    }
}
